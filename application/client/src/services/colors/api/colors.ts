import { client } from "services/api";

import { Color } from "../types/color";

const getColors = async () => {
  const { data } = await client.get<Color[]>("/colors/");

  return data;
};

export { getColors };
