import { ComponentProps, FC, useEffect, useMemo, useState } from "react";
import { Select } from "antd";

import { getColors } from "../../api/colors";
import { Color } from "../../types/color";

type Props = ComponentProps<typeof Select>;

const ColorSelect: FC<Props> = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [colors, setColors] = useState<Color[]>([]);

  const options = useMemo(() => {
    return colors.map((color) => ({ label: color.name, value: color.id }));
  }, [colors]);

  const fetchColors = async () => {
    setIsLoading(true);

    try {
      const newColors = await getColors();
      setColors(newColors);
    } catch {
      /* empty */
    }

    setIsLoading(false);
  };

  useEffect(() => {
    void fetchColors();
  }, []);

  return (
    <Select
      placeholder="Выберите цвет"
      options={options}
      disabled={isLoading}
      loading={isLoading}
      {...props}
    />
  );
};

export { ColorSelect };
