interface Order {
  offerId: number;
  clientId: number;
  rentalBegin: string;
  rentalEnd: string | null;
}

export { Order };
