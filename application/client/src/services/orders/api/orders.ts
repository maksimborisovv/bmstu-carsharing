import { client } from "services/api";

import { Order } from "../types/order";

const createOrder = async (order: Order) => {
  await client.post("/orders/start", order);
};

const updateOrder = async (offerId: number, datetime: Date) => {
  await client.put(`/orders/end/${offerId}`, {
    rentalEnd: datetime.toISOString(),
  });
};

export { createOrder, updateOrder };
