interface Car {
  categoryId: number;
  colorId: number;
  description: string;
  documents: string;
  id: number;
  model: string;
  ownerId: number;
  plateNumber: string;
  prodYear: number;
}

export { Car };
