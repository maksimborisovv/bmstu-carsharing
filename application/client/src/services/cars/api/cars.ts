import { client } from "services/api";

import { Car } from "../types/car";

const getCars = async () => {
  const { data } = await client.get<Car[]>("/cars");

  return data;
};

const getCar = async (id: number) => {
  const { data } = await client.get<Car>(`/cars/${id}`);

  return data;
};

const createCar = async (car: Partial<Car>) => {
  await client.post("/cars", car);
};

const deleteCar = async (id: number) => {
  await client.delete(`/cars/${id}`);
};

export { getCars, createCar, getCar, deleteCar };
