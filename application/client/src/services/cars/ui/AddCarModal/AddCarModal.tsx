import { Button, DatePicker, Form, Input, message, Modal } from "antd";
import { ComponentProps, FC } from "react";
import { Moment } from "moment";

import { ColorSelect } from "services/colors";
import { CategorySelect } from "services/categories";

import { createCar } from "../../api/cars";

type Props = ComponentProps<typeof Modal>;

interface FormValues {
  model: string;
  prodYear: Moment;
  colorId: number;
  plateNumber: string;
  description: string;
}

const AddCarModal: FC<Props> = ({ onCancel, ...props }) => {
  const [form] = Form.useForm<FormValues>();

  const onFinish = async (values: FormValues) => {
    try {
      await createCar({
        ...values,
        prodYear: values.prodYear.year(),
        ownerId: 1,
      });
    } catch {
      message.error("Не удалось добавить автомобиль.");
    }
  };

  return (
    <Modal
      title="Добавление автомобиля"
      footer={[
        <Button type="primary" onClick={form.submit} key="submit">
          Добавить
        </Button>,
        <Button onClick={onCancel} key="cancel">
          Отменить
        </Button>,
      ]}
      onCancel={onCancel}
      destroyOnClose
      {...props}
    >
      <Form form={form} onFinish={onFinish} preserve={false} layout="vertical">
        <Form.Item
          label="Категория"
          name="categoryId"
          rules={[{ required: true }]}
        >
          <CategorySelect />
        </Form.Item>
        <Form.Item label="Модель" name="model" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item
          label="Год выпуска"
          name="prodYear"
          rules={[{ required: true }]}
        >
          <DatePicker picker="year" style={{ width: "100%" }} />
        </Form.Item>
        <Form.Item
          label="Цвет автомобиля"
          name="colorId"
          rules={[{ required: true }]}
        >
          <ColorSelect />
        </Form.Item>
        <Form.Item
          label="Гос. номер"
          name="plateNumber"
          rules={[{ required: true }]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="Описание" name="description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          label="Документы"
          name="documents"
          rules={[{ required: true }]}
        >
          <Input.TextArea />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export { AddCarModal };
