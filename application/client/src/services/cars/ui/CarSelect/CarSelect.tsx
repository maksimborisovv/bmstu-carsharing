import { ComponentProps, FC, useEffect, useMemo, useState } from "react";
import { Select } from "antd";

import { getCars } from "../../api/cars";
import { Car } from "../../types/car";

type Props = ComponentProps<typeof Select>;

const CarSelect: FC<Props> = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [cars, setCars] = useState<Car[]>([]);

  const options = useMemo(() => {
    return cars.map((car) => ({
      label: `${car.model} ${car.prodYear} года выпуска`,
      value: car.id,
    }));
  }, [cars]);

  const fetchCars = async () => {
    setIsLoading(true);

    try {
      const newCars = await getCars();
      setCars(newCars);
    } catch {
      /* empty */
    }

    setIsLoading(false);
  };

  useEffect(() => {
    void fetchCars();
  }, []);

  return (
    <Select
      placeholder="Выберите автомобиль"
      options={options}
      disabled={isLoading}
      loading={isLoading}
      {...props}
    />
  );
};

export { CarSelect };
