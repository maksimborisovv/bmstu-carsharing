import { ComponentProps, FC } from "react";
import { Button, Table } from "antd";

import { Car } from "../../types/car";

interface Props extends Omit<ComponentProps<typeof Table<Car>>, "columns"> {
  data: Car[];
  onDelete?: (arg0: number) => void;
}

const CarsTable: FC<Props> = ({ data, onDelete, ...props }) => {
  return (
    <Table
      dataSource={data}
      rowKey="id"
      pagination={false}
      scroll={{ x: true }}
      {...props}
    >
      <Table.Column title="Модель" dataIndex="model" />
      <Table.Column title="Номерной знак" dataIndex="plateNumber" />
      <Table.Column title="Описание" dataIndex="description" />
      <Table.Column title="Год выпуска" dataIndex="prodYear" />
      <Table.Column
        title="Действия"
        render={(_, record: Car) => (
          <Button type="primary" danger onClick={() => onDelete?.(record.id)}>
            Удалить
          </Button>
        )}
      />
    </Table>
  );
};

export { CarsTable };
