interface Offer {
  availability: boolean;
  carId: number;
  id: number;
  ownerId: number;
  price: number;
}

export { Offer };
