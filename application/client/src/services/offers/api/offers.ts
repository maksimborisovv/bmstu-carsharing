import { client } from "services/api";

import { Offer } from "../types/offer";

const getOffers = async () => {
  const { data } = await client.get<Offer[]>("/offers/");

  return data;
};

const createOffer = async (offer: Partial<Offer>) => {
  await client.post("/offers", offer);
};

const deleteOffer = async (id: number) => {
  await client.delete(`/offers/${id}`);
};

export { getOffers, createOffer, deleteOffer };
