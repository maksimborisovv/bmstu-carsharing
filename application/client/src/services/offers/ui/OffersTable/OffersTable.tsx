import { ComponentProps, FC, useEffect, useState } from "react";
import { Button, Skeleton, Table } from "antd";

import { Car, getCar } from "services/cars";

import { Offer } from "../../types/offer";

interface Props extends Omit<ComponentProps<typeof Table<Offer>>, "columns"> {
  data: Offer[];
  onRentalBeginClick: (id: number) => void;
  onRentalEndClick: (id: number) => void;
  onDeleteClick: (id: number) => void;
}

const OffersTable: FC<Props> = ({
  data,
  onRentalBeginClick,
  onRentalEndClick,
  onDeleteClick,
  ...props
}) => {
  return (
    <Table
      dataSource={data}
      rowKey="id"
      pagination={false}
      scroll={{ x: true }}
      {...props}
    >
      <Table.Column
        title="Автомобиль"
        width="40%"
        dataIndex="carId"
        render={(_, offer: Offer) => <CarCell offer={offer} />}
      />
      <Table.Column title="Цена" dataIndex="price" />
      <Table.Column
        title="Действия"
        width="30%"
        render={(_, offer: Offer) => (
          <>
            <Button
              type="primary"
              danger={!offer.availability}
              onClick={() =>
                offer.availability
                  ? onRentalBeginClick(offer.id)
                  : onRentalEndClick(offer.id)
              }
            >
              {offer.availability ? "Взять в аренду" : "Завершить аренду"}
            </Button>
            <Button
              type="primary"
              danger
              onClick={() => onDeleteClick(offer.id)}
            >
              Удалить
            </Button>
          </>
        )}
      />
    </Table>
  );
};

const CarCell: FC<{ offer: Offer }> = ({ offer }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [car, setCar] = useState<Car | undefined>();

  const fetchCar = async () => {
    setIsLoading(true);

    try {
      const newCar = await getCar(offer.carId);
      setCar(newCar);
    } catch {
      /* empty */
    }

    setIsLoading(false);
  };

  useEffect(() => {
    void fetchCar();
  }, []);

  return isLoading ? <Skeleton paragraph={false} /> : <>{car?.model}</>;
};

export { OffersTable };
