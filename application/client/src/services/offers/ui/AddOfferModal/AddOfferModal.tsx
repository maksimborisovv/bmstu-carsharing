import { Button, Form, InputNumber, message, Modal } from "antd";
import { ComponentProps, FC } from "react";
import { Moment } from "moment";

import { CarSelect } from "services/cars";
import { createOffer } from "services/offers/api/offers";

type Props = ComponentProps<typeof Modal>;

interface FormValues {
  carId: number;
  price: number;
}

const AddOfferModal: FC<Props> = ({ onCancel, ...props }) => {
  const [form] = Form.useForm<FormValues>();

  const onFinish = async (values: FormValues) => {
    try {
      console.log(values);
      await createOffer({ ...values, ownerId: 1, availability: true });
    } catch {
      message.error("Не удалось добавить предложение.");
    }
  };

  return (
    <Modal
      title="Добавление предложения"
      footer={[
        <Button type="primary" onClick={form.submit} key="submit">
          Добавить
        </Button>,
        <Button onClick={onCancel} key="cancel">
          Отменить
        </Button>,
      ]}
      onCancel={onCancel}
      destroyOnClose
      {...props}
    >
      <Form form={form} onFinish={onFinish} preserve={false} layout="vertical">
        <Form.Item label="Автомобиль" name="carId" rules={[{ required: true }]}>
          <CarSelect />
        </Form.Item>
        <Form.Item label="Цена" name="price" rules={[{ required: true }]}>
          <InputNumber style={{ width: "100%" }} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export { AddOfferModal };
