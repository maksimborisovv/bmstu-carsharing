import { client } from "services/api";

import { Category } from "../types/category";

const getCategories = async () => {
  const { data } = await client.get<Category[]>("/categories/");

  return data;
};

export { getCategories };
