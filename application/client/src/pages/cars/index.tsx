import { Button, message } from "antd";
import { useEffect, useState } from "react";

import { Car, getCars, AddCarModal, CarsTable, deleteCar } from "services/cars";

import { Layout } from "components/Layout";

const CarsPage = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [cars, setCars] = useState<Car[]>([]);

  const fetchCars = async () => {
    setIsLoading(true);

    try {
      const newCars = await getCars();
      setCars(newCars);
    } catch (error) {
      message.error("Не удалось загрузить список автомобилей.");
      console.error(error);
    }

    setIsLoading(false);
  };

  const onDelete = async (id: number) => {
    setIsLoading(true);

    try {
      await deleteCar(id);
      message.success("Автомобиль успешно удалён.");

      await fetchCars();
    } catch (error) {
      message.error("Не удалось удалить автомобиль.");
      console.error(error);
    }

    setIsLoading(false);
  };

  useEffect(() => {
    void fetchCars();
  }, []);

  return (
    <Layout title="Автомобили">
      <Button type="primary" onClick={() => setIsModalOpen(true)}>
        Добавить
      </Button>
      <CarsTable loading={isLoading} data={cars} onDelete={onDelete} />
      <AddCarModal open={isModalOpen} onCancel={() => setIsModalOpen(false)} />
    </Layout>
  );
};

export default CarsPage;
