import { Button, message } from "antd";
import { useEffect, useState } from "react";

import { Layout } from "components/Layout";
import {
  AddOfferModal,
  OffersTable,
  Offer,
  getOffers,
  deleteOffer,
} from "services/offers";
import { createOrder, updateOrder } from "services/orders";

const OffersPage = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [offers, setOffers] = useState<Offer[]>([]);

  const fetchOffers = async () => {
    setIsLoading(true);

    try {
      const newOffers = await getOffers();
      setOffers(newOffers);
    } catch (error) {
      message.error("Не удалось загрузить список предложений.");
      console.error(error);
    }

    setIsLoading(false);
  };

  const beginRental = async (offerId: number) => {
    setIsLoading(true);

    try {
      await createOrder({
        offerId,
        clientId: 1,
        rentalBegin: new Date().toISOString(),
        rentalEnd: null,
      });
      await fetchOffers();
    } catch {
      // lalala
    }

    setIsLoading(false);
  };

  const endRental = async (offerId: number) => {
    setIsLoading(true);

    try {
      await updateOrder(offerId, new Date());
      await fetchOffers();
    } catch {
      // lalala
    }

    setIsLoading(false);
  };

  const deleteOfferClick = async (offerId: number) => {
    setIsLoading(true);

    try {
      await deleteOffer(offerId);
      await fetchOffers();
    } catch {
      // lalalal
    }

    setIsLoading(false);
  };

  useEffect(() => {
    void fetchOffers();
  }, []);

  return (
    <Layout title="Предложения">
      <Button type="primary" onClick={() => setIsModalOpen(true)}>
        Добавить
      </Button>
      <OffersTable
        data={offers}
        loading={isLoading}
        onRentalBeginClick={beginRental}
        onRentalEndClick={endRental}
        onDeleteClick={deleteOfferClick}
      />
      <AddOfferModal
        open={isModalOpen}
        onCancel={() => setIsModalOpen(false)}
      />
    </Layout>
  );
};

export default OffersPage;
