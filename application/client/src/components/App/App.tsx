import { FC, lazy, Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ConfigProvider, Spin } from "antd";
import ruRU from "antd/locale/ru_RU";

const CarsPage = lazy(() => import("pages/cars"));
const OffersPage = lazy(() => import("pages/offers"));

const App: FC = () => {
  return (
    <ConfigProvider locale={ruRU}>
      <BrowserRouter>
        <Suspense fallback={<Spin />}>
          <Routes>
            <Route path="/cars/" element={<CarsPage />} />
            <Route path="/offers/" element={<OffersPage />} />
          </Routes>
        </Suspense>
      </BrowserRouter>
    </ConfigProvider>
  );
};

export { App };
