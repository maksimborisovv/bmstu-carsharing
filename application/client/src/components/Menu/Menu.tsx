import { ComponentProps, FC } from "react";
import { useNavigate } from "react-router-dom";
import { Menu as AntMenu, MenuProps } from "antd";
import { CarOutlined, WalletOutlined } from "@ant-design/icons";
import { ItemType } from "antd/es/menu/hooks/useItems";

type Props = ComponentProps<typeof AntMenu>;

const ITEMS: ItemType[] = [
  { key: "cars", label: "Автомобили", icon: <CarOutlined /> },
  { key: "offers", label: "Предложения", icon: <WalletOutlined /> },
];

const Menu: FC<Props> = (props) => {
  const navigate = useNavigate();

  const onClick: MenuProps["onClick"] = (e) => {
    switch (e.key) {
      case "cars":
        navigate("/cars/");
        break;
      case "offers":
        navigate("/offers/");
        break;
    }
  };

  return (
    <AntMenu
      mode="horizontal"
      items={ITEMS}
      selectedKeys={[]}
      onClick={onClick}
      {...props}
    />
  );
};

export { Menu };
