package com.carsharing.service;

import com.carsharing.entity.OfferEntity;
import com.carsharing.entity.OrderEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

@Tag("ServiceTestUnit")
@SpringBootTest(classes = OrderService.class)
class OrderServiceTest {
    @Autowired
    private OrderService service;

    @MockBean
    private IOrderRepo repo;

    @Test
    void createOrder() {
        OrderEntity order = OrderEntity.builder().build();

        int expId = 1;
        Mockito.when(repo.createOrder(order)).thenReturn(expId);

        int id = service.createOrder(order);

        Assertions.assertEquals(expId, id);
        Mockito.verify(repo).createOrder(order);
    }

    @Test
    void updateOrder() {
        OrderEntity order = OrderEntity.builder().build();

        int id = 1;

        service.updateOrder(order, id);

        Assertions.assertEquals(id, order.getId());
        Mockito.verify(repo).updateOrder(order);
    }

    @Test
    void deleteOrder() {
        int id = 1;

        service.deleteOrder(id);

        Mockito.verify(repo).deleteOrder(id);
    }

    @Test
    void getOrder() {
        int id = 1;
        OrderEntity order = OrderEntity.builder().id(id).build();

        Mockito.when(repo.getOrder(id)).thenReturn(order);

        OrderEntity res = service.getOrder(id);

        Assertions.assertEquals(order, res);
        Mockito.verify(repo).getOrder(id);
    }

    @Test
    void getOrderList() {
        ArrayList<OrderEntity> orderEntities = new ArrayList<>();
        orderEntities.add(
                OrderEntity.builder().build()
        );

        Mockito.when(repo.getOrderList()).thenReturn(orderEntities);

        ArrayList<OrderEntity> res = service.getOrderList();

        Assertions.assertArrayEquals(orderEntities.toArray(), res.toArray());
        Mockito.verify(repo).getOrderList();
    }

    @Test
    void getOrderByClient() {
        ArrayList<OrderEntity> orderEntities = new ArrayList<>();
        orderEntities.add(
                OrderEntity.builder().build()
        );

        int clientId = 1;
        Mockito.when(repo.getOrdersByClient(clientId)).thenReturn(orderEntities);

        ArrayList<OrderEntity> res = service.getOrderByClient(clientId);

        Assertions.assertArrayEquals(orderEntities.toArray(), res.toArray());
        Mockito.verify(repo).getOrdersByClient(clientId);
    }
}