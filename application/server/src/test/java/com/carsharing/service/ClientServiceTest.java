package com.carsharing.service;

import com.carsharing.entity.ClientEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

@Tag("ServiceTestUnit")
@SpringBootTest(classes = ClientService.class)
class ClientServiceTest {
    @Autowired
    private ClientService service;

    @MockBean
    private IClientRepo repo;

    @Test
    void createClient() {
        String password = "Qwerty123";
        ClientEntity client = ClientEntity.builder().password(password).build();

        int expId = 1;
        Mockito.when(repo.createClient(client)).thenReturn(expId);

        int id = service.createClient(client);

        Assertions.assertEquals(expId, id);
        Assertions.assertEquals("2af9b1ba42dc5eb01743e6b3759b6e4b", client.getPassword());
        Mockito.verify(repo).createClient(client);
    }

    @Test
    void updateClient() {
        ClientEntity client = ClientEntity.builder().build();

        int id = 1;

        service.updateClient(client, id);

        Assertions.assertEquals(id, client.getId());
        Mockito.verify(repo).updateClient(client);
    }

    @Test
    void deleteClient() {
        int id = 1;

        service.deleteClient(id);

        Mockito.verify(repo).deleteClient(id);
    }

    @Test
    void getClient() {
        int id = 1;
        ClientEntity client = ClientEntity.builder().id(id).build();

        Mockito.when(repo.getClient(id)).thenReturn(client);

        ClientEntity res = service.getClient(id);

        Assertions.assertEquals(client, res);
        Mockito.verify(repo).getClient(id);
    }

    @Test
    void getClientList() {
        ArrayList<ClientEntity> clientEntities = new ArrayList<>();

        clientEntities.add(new ClientEntity(1, "FirstName", "LastName",
                "MiddleName", "81231231212", "email@mail.ru",
                1, "1234 123456", "12 12 123456", "Password"));

        Mockito.when(repo.getClientList()).thenReturn(clientEntities);

        ArrayList<ClientEntity> res = service.getClientList();

        Assertions.assertArrayEquals(clientEntities.toArray(), res.toArray());
        Mockito.verify(repo).getClientList();
    }

    @Test
    void logInClient() {
    }
}