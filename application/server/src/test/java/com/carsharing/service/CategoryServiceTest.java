package com.carsharing.service;

import com.carsharing.entity.CategoryEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

@Tag("ServiceUnitTest")
@SpringBootTest(classes = CategoryService.class)
class CategoryServiceTest {
    @Autowired
    private CategoryService service;

    @MockBean
    private ICategoryRepo repo;

    @Test
    void createCategory() {
        CategoryEntity category = CategoryEntity.builder().build();

        int expId = 1;
        Mockito.when(repo.createCategory(category)).thenReturn(expId);

        int id = service.createCategory(category);

        Assertions.assertEquals(expId, id);
        Mockito.verify(repo).createCategory(category);
    }

    @Test
    void updateCategory() {
        CategoryEntity category = CategoryEntity.builder().build();

        int id = 1;

        service.updateCategory(category, id);

        Assertions.assertEquals(id, category.getId());
        Mockito.verify(repo).updateCategory(category);
    }

    @Test
    void deleteCategory() {
        int id = 1;

        service.deleteCategory(id);

        Mockito.verify(repo).deleteCategory(id);
    }

    @Test
    void getCategory() {
        int id = 1;
        CategoryEntity category = CategoryEntity.builder().id(id).build();

        Mockito.when(repo.getCategory(id)).thenReturn(category);

        CategoryEntity res = service.getCategory(id);

        Assertions.assertEquals(category, res);
        Mockito.verify(repo).getCategory(id);
    }

    @Test
    void getCategoryList() {
        ArrayList<CategoryEntity> categoryEntities = new ArrayList<>();
        categoryEntities.add(
                CategoryEntity.builder()
                        .id(1)
                        .name("Name")
                        .build()
        );

        Mockito.when(repo.getCategoryList()).thenReturn(categoryEntities);

        ArrayList<CategoryEntity> res = service.getCategoryList();

        Assertions.assertArrayEquals(categoryEntities.toArray(), res.toArray());
        Mockito.verify(repo).getCategoryList();
    }
}