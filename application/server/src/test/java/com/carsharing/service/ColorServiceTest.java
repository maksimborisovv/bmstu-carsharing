package com.carsharing.service;

import com.carsharing.entity.ColorEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

@Tag("ServiceTestUnit")
@SpringBootTest(classes = ColorService.class)
class ColorServiceTest {
    @Autowired
    private ColorService service;

    @MockBean
    private IColorRepo repo;

    @Test
    void createColor() {
        ColorEntity color = ColorEntity.builder().build();

        int expId = 1;
        Mockito.when(repo.createColor(color)).thenReturn(expId);

        int id = service.createColor(color);

        Assertions.assertEquals(expId, id);
        Mockito.verify(repo).createColor(color);
    }

    @Test
    void updateColor() {
        ColorEntity color = ColorEntity.builder().build();

        int id = 1;

        service.updateColor(color, id);

        Assertions.assertEquals(id, color.getId());
        Mockito.verify(repo).updateColor(color);
    }

    @Test
    void deleteColor() {
        int id = 1;

        service.deleteColor(id);

        Mockito.verify(repo).deleteColor(id);
    }

    @Test
    void getColor() {
        int id = 1;
        ColorEntity color = ColorEntity.builder().id(id).build();

        Mockito.when(repo.getColor(id)).thenReturn(color);

        ColorEntity res = service.getColor(id);

        Assertions.assertEquals(color, res);
        Mockito.verify(repo).getColor(id);
    }

    @Test
    void getColorList() {
        ArrayList<ColorEntity> colorEntities = new ArrayList<>();
        colorEntities.add(
                ColorEntity.builder()
                        .id(1)
                        .name("Name")
                        .build()
        );

        Mockito.when(repo.getColorList()).thenReturn(colorEntities);

        ArrayList<ColorEntity> res = service.getColorList();

        Assertions.assertArrayEquals(colorEntities.toArray(), res.toArray());
        Mockito.verify(repo).getColorList();
    }
}