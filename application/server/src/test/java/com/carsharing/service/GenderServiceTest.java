package com.carsharing.service;

import com.carsharing.entity.GenderEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

@Tag("ServiceTestUnit")
@SpringBootTest(classes = GenderService.class)
class GenderServiceTest {
    @Autowired
    private GenderService service;

    @MockBean
    private IGenderRepo repo;

    @Test
    void createGender() {
        GenderEntity gender = GenderEntity.builder().build();

        int expId = 1;
        Mockito.when(repo.createGender(gender)).thenReturn(expId);

        int id = service.createGender(gender);

        Assertions.assertEquals(expId, id);
        Mockito.verify(repo).createGender(gender);
    }

    @Test
    void updateGender() {
        GenderEntity gender = GenderEntity.builder().build();

        int id = 1;

        service.updateGender(gender, id);

        Assertions.assertEquals(id, gender.getId());
        Mockito.verify(repo).updateGender(gender);
    }

    @Test
    void deleteGender() {
        int id = 1;

        service.deleteGender(id);

        Mockito.verify(repo).deleteGender(id);
    }

    @Test
    void getGender() {
        int id = 1;
        GenderEntity gender = GenderEntity.builder().id(id).build();

        Mockito.when(repo.getGender(id)).thenReturn(gender);

        GenderEntity res = service.getGender(id);

        Assertions.assertEquals(gender, res);
        Mockito.verify(repo).getGender(id);
    }

    @Test
    void getGenderList() {
        ArrayList<GenderEntity> genderEntities = new ArrayList<>();
        genderEntities.add(
                GenderEntity.builder()
                        .id(1)
                        .name("Name")
                        .build()
        );

        Mockito.when(repo.getGenderList()).thenReturn(genderEntities);

        ArrayList<GenderEntity> res = service.getGenderList();

        Assertions.assertArrayEquals(genderEntities.toArray(), res.toArray());
        Mockito.verify(repo).getGenderList();
    }
}