package com.carsharing.service;

import com.carsharing.entity.OfferEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

@Tag("ServiceTestUnit")
@SpringBootTest(classes = OfferService.class)
class OfferServiceTest {
    @Autowired
    private OfferService service;

    @MockBean
    private IOfferRepo repo;

    @Test
    void createOffer() {
        OfferEntity offer = OfferEntity.builder().build();

        int expId = 1;
        Mockito.when(repo.createOffer(offer)).thenReturn(expId);

        int id = service.createOffer(offer);

        Assertions.assertEquals(expId, id);
        Mockito.verify(repo).createOffer(offer);
    }

    @Test
    void updateOffer() {
        OfferEntity offer = OfferEntity.builder().build();

        int id = 1;

        service.updateOffer(offer, id);

        Assertions.assertEquals(id, offer.getId());
        Mockito.verify(repo).updateOffer(offer);
    }

    @Test
    void deleteOffer() {
        int id = 1;

        service.deleteOffer(id);

        Mockito.verify(repo).deleteOffer(id);
    }

    @Test
    void getOffer() {
        int id = 1;
        OfferEntity offer = OfferEntity.builder().id(id).build();

        Mockito.when(repo.getOffer(id)).thenReturn(offer);

        OfferEntity res = service.getOffer(id);

        Assertions.assertEquals(offer, res);
        Mockito.verify(repo).getOffer(id);
    }

    @Test
    void getOfferList() {
        ArrayList<OfferEntity> offerEntities = new ArrayList<>();
        offerEntities.add(
                OfferEntity.builder().build()
        );

        Mockito.when(repo.getOfferList()).thenReturn(offerEntities);

        ArrayList<OfferEntity> res = service.getOfferList();

        Assertions.assertArrayEquals(offerEntities.toArray(), res.toArray());
        Mockito.verify(repo).getOfferList();
    }

    @Test
    void getOfferListByOwner() {
        ArrayList<OfferEntity> offerEntities = new ArrayList<>();
        offerEntities.add(
                OfferEntity.builder().build()
        );

        int ownerId = 1;
        Mockito.when(repo.getOfferListByOwner(ownerId)).thenReturn(offerEntities);

        ArrayList<OfferEntity> res = service.getOfferListByOwner(ownerId);

        Assertions.assertArrayEquals(offerEntities.toArray(), res.toArray());
        Mockito.verify(repo).getOfferListByOwner(ownerId);
    }
}