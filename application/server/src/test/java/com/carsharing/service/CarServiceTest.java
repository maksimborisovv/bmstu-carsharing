package com.carsharing.service;

import com.carsharing.entity.CarEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

@Tag("ServiceTestUnit")
@SpringBootTest(classes = CarService.class)
class CarServiceTest {
    @Autowired
    private CarService service;

    @MockBean
    private ICarRepo repo;

    @Test
    void createCar() {
        CarEntity car = CarEntity.builder().build();

        int expId = 1;
        Mockito.when(repo.createCar(car)).thenReturn(expId);

        int id = service.createCar(car);

        Assertions.assertEquals(expId, id);
        Mockito.verify(repo).createCar(car);
    }

    @Test
    void updateCar() {
        CarEntity car = CarEntity.builder().build();

        int id = 1;

        service.updateCar(car, id);

        Assertions.assertEquals(id, car.getId());
        Mockito.verify(repo).updateCar(car);
    }

    @Test
    void deleteCar() {
        int id = 1;

        service.deleteCar(id);

        Mockito.verify(repo).deleteCar(id);
    }

    @Test
    void getCar() {
        int id = 1;
        CarEntity car = CarEntity.builder().id(id).build();

        Mockito.when(repo.getCar(id)).thenReturn(car);

        CarEntity res = service.getCar(id);

        Assertions.assertEquals(car, res);
        Mockito.verify(repo).getCar(id);
    }

    @Test
    void getCarList() {
        ArrayList<CarEntity> carEntities = new ArrayList<>();
        carEntities.add(
                CarEntity.builder()
                        .id(1)
                        .model("Model")
                        .plateNumber("0ААА00 000")
                        .prodYear(2022)
                        .colorId(1)
                        .categoryId(1)
                        .description("Description")
                        .documents("Documents")
                        .ownerId(1)
                        .build()
        );

        Mockito.when(repo.getCarList()).thenReturn(carEntities);

        ArrayList<CarEntity> res = service.getCarList();

        Assertions.assertArrayEquals(carEntities.toArray(), res.toArray());
        Mockito.verify(repo).getCarList();
    }

    @Test
    void getCarListByOwner() {
        ArrayList<CarEntity> carEntities = new ArrayList<>();
        carEntities.add(
                CarEntity.builder()
                        .id(1)
                        .model("Model")
                        .plateNumber("0ААА00 000")
                        .prodYear(2022)
                        .colorId(1)
                        .categoryId(1)
                        .description("Description")
                        .documents("Documents")
                        .ownerId(1)
                        .build()
        );

        int ownerId = 1;
        Mockito.when(repo.getCarListByOwner(ownerId)).thenReturn(carEntities);

        ArrayList<CarEntity> res = service.getCarListByOwner(ownerId);

        Assertions.assertArrayEquals(carEntities.toArray(), res.toArray());
        Mockito.verify(repo).getCarListByOwner(ownerId);
    }
}