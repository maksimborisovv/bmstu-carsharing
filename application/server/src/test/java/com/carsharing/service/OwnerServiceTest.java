package com.carsharing.service;

import com.carsharing.entity.ClientEntity;
import com.carsharing.entity.OwnerEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

@Tag("ServiceTestUnit")
@SpringBootTest(classes = OwnerService.class)
class OwnerServiceTest {
    @Autowired
    private OwnerService service;

    @MockBean
    private IOwnerRepo repo;

    @Test
    void createOwner() {
        String password = "Qwerty123";
        OwnerEntity owner = OwnerEntity.builder().password(password).build();

        int expId = 1;
        Mockito.when(repo.createOwner(owner)).thenReturn(expId);

        int id = service.createOwner(owner);

        Assertions.assertEquals(expId, id);
        Assertions.assertEquals("2af9b1ba42dc5eb01743e6b3759b6e4b", owner.getPassword());
        Mockito.verify(repo).createOwner(owner);
    }

    @Test
    void updateOwner() {
        OwnerEntity owner = OwnerEntity.builder().build();

        int id = 1;

        service.updateOwner(owner, id);

        Assertions.assertEquals(id, owner.getId());
        Mockito.verify(repo).updateOwner(owner);
    }

    @Test
    void deleteOwner() {
        int id = 1;

        service.deleteOwner(id);

        Mockito.verify(repo).deleteOwner(id);
    }

    @Test
    void getOwner() {
        int id = 1;
        OwnerEntity owner = OwnerEntity.builder().id(id).build();

        Mockito.when(repo.getOwner(id)).thenReturn(owner);

        OwnerEntity res = service.getOwner(id);

        Assertions.assertEquals(owner, res);
        Mockito.verify(repo).getOwner(id);
    }

    @Test
    void getOwnerList() {
        ArrayList<OwnerEntity> ownerEntities = new ArrayList<>();

        ownerEntities.add(
                OwnerEntity.builder().build()
        );

        Mockito.when(repo.getOwnerList()).thenReturn(ownerEntities);

        ArrayList<OwnerEntity> res = service.getOwnerList();

        Assertions.assertArrayEquals(ownerEntities.toArray(), res.toArray());
        Mockito.verify(repo).getOwnerList();
    }
}