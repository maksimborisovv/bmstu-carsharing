package com.carsharing.starter;

import com.carsharing.starter.util.ConnectionManager;

import java.sql.Connection;

public class Runner {
    public static Connection run() {
        try {
            return ConnectionManager.open();
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }
}
