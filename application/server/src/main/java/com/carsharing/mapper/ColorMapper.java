package com.carsharing.mapper;

import com.carsharing.dto.ColorDTORequest;
import com.carsharing.dto.ColorDTOResponse;
import com.carsharing.entity.ColorEntity;
import org.springframework.stereotype.Component;

@Component
public class ColorMapper {
    public ColorEntity toEntity(ColorDTOResponse dto) {
        if (dto == null) return null;

        return new ColorEntity(dto.id(), dto.name());
    }

    public ColorDTOResponse toDTOResponse(ColorEntity entity) {
        if (entity == null) return null;

        return new ColorDTOResponse(entity.getId(), entity.getName());
    }

    public ColorEntity toEntity(ColorDTORequest dto) {
        if (dto == null) return null;

        return new ColorEntity(dto.name());
    }

    public ColorDTORequest toDTORequest(ColorEntity entity) {
        if (entity == null) return null;

        return new ColorDTORequest(entity.getName());
    }
}
