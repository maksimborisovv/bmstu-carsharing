package com.carsharing.mapper;

import com.carsharing.dto.OfferDTORequest;
import com.carsharing.dto.OfferDTOResponse;
import com.carsharing.entity.OfferEntity;
import org.springframework.stereotype.Component;

@Component
public class OfferMapper {
    public OfferEntity toEntity(OfferDTOResponse dto) {
        if (dto == null) return null;

        return new OfferEntity(
                dto.id(),
                dto.carId(),
                dto.ownerId(),
                dto.availability()
        );
    }

    public OfferDTOResponse toDTOResponse(OfferEntity entity) {
        if (entity == null) return null;

        return new OfferDTOResponse(
                entity.getId(),
                entity.getCarId(),
                entity.getOwnerId(),
                entity.getPrice(),
                entity.getAvailability()
        );
    }

    public OfferEntity toEntity(OfferDTORequest dto) {
        if (dto == null) return null;

        return new OfferEntity(
                dto.carId(),
                dto.ownerId(),
                dto.price(),
                dto.availability()
        );
    }

    public OfferDTORequest toDTORequest(OfferEntity entity) {
        if (entity == null) return null;

        return new OfferDTORequest(
                entity.getCarId(),
                entity.getOwnerId(),
                entity.getPrice(),
                entity.getAvailability()
        );
    }
}
