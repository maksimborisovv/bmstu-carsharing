package com.carsharing.mapper;

import com.carsharing.dto.GenderDTORequest;
import com.carsharing.dto.GenderDTOResponse;
import com.carsharing.entity.GenderEntity;
import org.springframework.stereotype.Component;

@Component
public class GenderMapper {
    public GenderEntity toEntity(GenderDTOResponse dto) {
        if (dto == null) return null;

        return new GenderEntity(dto.id(), dto.name());
    }

    public GenderDTOResponse toDTOResponse(GenderEntity entity) {
        if (entity == null) return null;

        return new GenderDTOResponse(entity.getId(), entity.getName());
    }

    public GenderEntity toEntity(GenderDTORequest dto) {
        if (dto == null) return null;

        return new GenderEntity(dto.name());
    }

    public GenderDTORequest toDTORequest(GenderEntity entity) {
        if (entity == null) return null;

        return new GenderDTORequest(entity.getName());
    }
}
