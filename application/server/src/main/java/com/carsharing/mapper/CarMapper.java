package com.carsharing.mapper;

import com.carsharing.dto.CarDTORequest;
import com.carsharing.dto.CarDTOResponse;
import com.carsharing.entity.CarEntity;
import org.springframework.stereotype.Component;

@Component
public class CarMapper {
    public CarEntity toEntity(CarDTOResponse dto) {
        if (dto == null) return null;

        return new CarEntity(
                dto.id(),
                dto.model(),
                dto.plateNumber(),
                dto.prodYear(),
                dto.colorId(),
                dto.categoryId(),
                dto.description(),
                dto.documents(),
                dto.ownerId()
        );
    }

    public CarDTOResponse toDTOResponse(CarEntity entity) {
        if (entity == null) return null;

        return new CarDTOResponse(
                entity.getId(),
                entity.getModel(),
                entity.getPlateNumber(),
                entity.getProdYear(),
                entity.getColorId(),
                entity.getCategoryId(),
                entity.getDescription(),
                entity.getDocuments(),
                entity.getOwnerId()
        );
    }

    public CarEntity toEntity(CarDTORequest dto) {
        if (dto == null) return null;

        return new CarEntity(
                dto.model(),
                dto.plateNumber(),
                dto.prodYear(),
                dto.colorId(),
                dto.categoryId(),
                dto.description(),
                dto.documents(),
                dto.ownerId()
        );
    }

    public CarDTORequest toDTORequest(CarEntity entity) {
        if (entity == null) return null;

        return new CarDTORequest(
                entity.getModel(),
                entity.getPlateNumber(),
                entity.getProdYear(),
                entity.getColorId(),
                entity.getCategoryId(),
                entity.getDescription(),
                entity.getDocuments(),
                entity.getOwnerId()
        );
    }
}
