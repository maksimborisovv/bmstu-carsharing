package com.carsharing.mapper;

import com.carsharing.dto.ClientDTORequest;
import com.carsharing.dto.ClientDTOResponse;
import com.carsharing.entity.ClientEntity;
import org.springframework.stereotype.Component;

@Component
public class ClientMapper {
    public ClientEntity toEntity(ClientDTOResponse dto) {
        if (dto == null) return null;

        return new ClientEntity(
                dto.id(),
                dto.firstName(),
                dto.lastName(),
                dto.middleName(),
                dto.phone(),
                dto.email(),
                dto.genderId(),
                dto.passport(),
                dto.drivingLicense(),
                dto.password()
        );
    }

    public ClientDTOResponse toDTOResponse(ClientEntity entity) {
        if (entity == null) return null;

        return new ClientDTOResponse(
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getMiddleName(),
                entity.getPhone(),
                entity.getEmail(),
                entity.getGenderId(),
                entity.getPassport(),
                entity.getDrivingLicense(),
                entity.getPassword()
        );
    }

    public ClientEntity toEntity(ClientDTORequest dto) {
        if (dto == null) return null;

        return new ClientEntity(
                dto.firstName(),
                dto.lastName(),
                dto.middleName(),
                dto.phone(),
                dto.email(),
                dto.genderId(),
                dto.passport(),
                dto.drivingLicense(),
                dto.password()
        );
    }

    public ClientDTORequest toDTORequest(ClientEntity entity) {
        if (entity == null) return null;

        return new ClientDTORequest(
                entity.getFirstName(),
                entity.getLastName(),
                entity.getMiddleName(),
                entity.getPhone(),
                entity.getEmail(),
                entity.getGenderId(),
                entity.getPassport(),
                entity.getDrivingLicense(),
                entity.getPassword()
        );
    }
}
