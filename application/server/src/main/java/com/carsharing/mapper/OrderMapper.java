package com.carsharing.mapper;

import com.carsharing.dto.OrderDTORequest;
import com.carsharing.dto.OrderDTOResponse;
import com.carsharing.entity.OrderEntity;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper {
    public OrderEntity toEntity(OrderDTOResponse dto) {
        if (dto == null) return null;

        return new OrderEntity(
                dto.id(),
                dto.offerId(),
                dto.clientId(),
                dto.rentalBegin(),
                dto.rentalEnd()
        );
    }

    public OrderDTOResponse toDTOResponse(OrderEntity entity) {
        if (entity == null) return null;

        return new OrderDTOResponse(
                entity.getId(),
                entity.getOfferId(),
                entity.getClientId(),
                entity.getRentalBegin(),
                entity.getRentalEnd()
        );
    }

    public OrderEntity toEntity(OrderDTORequest dto) {
        if (dto == null) return null;

        return new OrderEntity(
                dto.offerId(),
                dto.clientId(),
                dto.rentalBegin(),
                dto.rentalEnd()
        );
    }

    public OrderDTORequest toDTORequest(OrderEntity entity) {
        if (entity == null) return null;

        return new OrderDTORequest(
                entity.getOfferId(),
                entity.getClientId(),
                entity.getRentalBegin(),
                entity.getRentalEnd()
        );
    }
}
