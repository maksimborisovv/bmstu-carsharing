package com.carsharing.mapper;

import com.carsharing.dto.CategoryDTORequest;
import com.carsharing.dto.CategoryDTOResponse;
import com.carsharing.entity.CategoryEntity;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {
    public CategoryEntity toEntity(CategoryDTOResponse dto) {
        if (dto == null) return null;

        return new CategoryEntity(dto.id(), dto.name());
    }

    public CategoryDTOResponse toDTOResponse(CategoryEntity entity) {
        if (entity == null) return null;

        return new CategoryDTOResponse(entity.getId(), entity.getName());
    }

    public CategoryEntity toEntity(CategoryDTORequest dto) {
        if (dto == null) return null;

        return new CategoryEntity(dto.name());
    }

    public CategoryDTORequest toDTORequest(CategoryEntity entity) {
        if (entity == null) return null;

        return new CategoryDTORequest(entity.getName());
    }
}
