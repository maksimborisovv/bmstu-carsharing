package com.carsharing.mapper;

import com.carsharing.dto.OwnerDTORequest;
import com.carsharing.dto.OwnerDTOResponse;
import com.carsharing.entity.OwnerEntity;
import org.springframework.stereotype.Component;

@Component
public class OwnerMapper {
    public OwnerEntity toEntity(OwnerDTOResponse dto) {
        if (dto == null) return null;

        return new OwnerEntity(
                dto.id(),
                dto.firstName(),
                dto.lastName(),
                dto.middleName(),
                dto.phone(),
                dto.email(),
                dto.genderId(),
                dto.rating(),
                dto.passport(),
                dto.password()
        );
    }

    public OwnerDTOResponse toDTOResponse(OwnerEntity entity) {
        if (entity == null) return null;

        return new OwnerDTOResponse(
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getMiddleName(),
                entity.getPhone(),
                entity.getEmail(),
                entity.getGenderId(),
                entity.getRating(),
                entity.getPassport(),
                entity.getPassword()
        );
    }

    public OwnerEntity toEntity(OwnerDTORequest dto) {
        if (dto == null) return null;

        return new OwnerEntity(
                dto.firstName(),
                dto.lastName(),
                dto.middleName(),
                dto.phone(),
                dto.email(),
                dto.genderId(),
                dto.rating(),
                dto.passport(),
                dto.password()
        );
    }

    public OwnerDTORequest toDTORequest(OwnerEntity entity) {
        if (entity == null) return null;

        return new OwnerDTORequest(
                entity.getFirstName(),
                entity.getLastName(),
                entity.getMiddleName(),
                entity.getPhone(),
                entity.getEmail(),
                entity.getGenderId(),
                entity.getRating(),
                entity.getPassport(),
                entity.getPassword()
        );
    }
}
