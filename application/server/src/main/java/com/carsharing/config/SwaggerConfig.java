package com.carsharing.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    public static final String CAR_TAG = "Cars";
    public static final String CATEGORY_TAG = "Categories";
    public static final String CLIENT_TAG = "Clients";
    public static final String COLOR_TAG = "Colors";
    public static final String GENDER_TAG = "Genders";
    public static final String OFFER_TAG = "Offers";
    public static final String ORDER_TAG = "Orders";
    public static final String OWNER_TAG = "Owners";

    @Bean
    public OpenAPI api() {
        return new OpenAPI()
                .info(new Info().title("Carsharing")
                        .version("2.0.0")
                        .description("Car rental service")
                        .contact(
                                new Contact()
                                        .email("maks499344@gmail.com")
                                        .name("Maksim Borisov")
                        ));
    }
}
