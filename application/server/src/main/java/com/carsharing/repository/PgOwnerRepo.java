package com.carsharing.repository;

import com.carsharing.entity.OwnerEntity;
import com.carsharing.service.IOwnerRepo;
import com.carsharing.starter.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class PgOwnerRepo implements IOwnerRepo {
    private final Connection pgConn = Runner.run();
    private final Logger logger = LoggerFactory.getLogger(PgOwnerRepo.class);

    @Override
    public int createOwner(OwnerEntity owner) {
        int resId;
        String sqlReq = """
                insert into owners (first_name, last_name, middle_name, phone, email, \
                gender_id, rating, passport, password) \
                values (?, ?, ?, ?, ?, ?, ?, ?, ?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setString(1, owner.getFirstName());
            statement.setString(2, owner.getLastName());
            statement.setString(3, owner.getMiddleName());
            statement.setString(4, owner.getPhone());
            statement.setString(5, owner.getEmail());
            statement.setInt(6, owner.getGenderId());
            statement.setDouble(7, owner.getRating());
            statement.setString(8, owner.getPassport());
            statement.setString(9, owner.getPassword());

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            logger.error("Unsuccessful owner creation");
            throw new RuntimeException(e);
        }

        logger.info("Owner was created");
        return resId;
    }

    @Override
    public void updateOwner(OwnerEntity owner) {
        String sqlReq = """
                update owners set (first_name, last_name, middle_name, phone, email, \
                gender_id, rating, passport, password) \
                = (?, ?, ?, ?, ?, ?, ?, ?, ?)
                where id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setString(1, owner.getFirstName());
            statement.setString(2, owner.getLastName());
            statement.setString(3, owner.getMiddleName());
            statement.setString(4, owner.getPhone());
            statement.setString(5, owner.getEmail());
            statement.setInt(6, owner.getGenderId());
            statement.setDouble(7, owner.getRating());
            statement.setString(8, owner.getPassport());
            statement.setString(9, owner.getPassword());
            statement.setInt(10, owner.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful owner update");
            throw new RuntimeException(e);
        }

        logger.info("Owner was updated");
    }

    @Override
    public void deleteOwner(int id) {
        String sqlReq = """
                delete from owners
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful owner deletion");
            throw new RuntimeException(e);
        }

        logger.info("Owner was deleted");
    }

    @Override
    public OwnerEntity getOwner(int id) {
        OwnerEntity owner = null;

        String sqlReq = """
                select * from owners
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                owner = new OwnerEntity(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("middle_name"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getInt("gender_id"),
                        resultSet.getDouble("rating"),
                        resultSet.getString("passport"),
                        resultSet.getString("password"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return owner;
    }

    @Override
    public ArrayList<OwnerEntity> getOwnerList() {
        ArrayList<OwnerEntity> ownerList = new ArrayList<>();

        String sqlReq = """
                select * from owners
                order by id
                """;

        try (Statement statement = pgConn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlReq);

            while (resultSet.next()) {
                OwnerEntity owner = new OwnerEntity(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("middle_name"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getInt("gender_id"),
                        resultSet.getDouble("rating"),
                        resultSet.getString("passport"),
                        resultSet.getString("password"));

                ownerList.add(owner);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return ownerList;
    }

    @Override
    public OwnerEntity logInOwner(OwnerEntity owner) {
        OwnerEntity logInOwner = null;

        String sqlReq = """
                select * from owners
                where email = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setString(1, owner.getEmail());

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                logInOwner = new OwnerEntity(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("middle_name"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getInt("gender_id"),
                        resultSet.getDouble("rating"),
                        resultSet.getString("passport"),
                        resultSet.getString("password"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if (logInOwner == null) {
            return null;
        }

        if (owner.getPassword().equals(logInOwner.getPassword())) {
            return logInOwner;
        } else {
            return null;
        }
    }
}
