package com.carsharing.repository;

import com.carsharing.entity.ClientEntity;
import com.carsharing.service.IClientRepo;
import com.carsharing.starter.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class PgClientRepo implements IClientRepo {
    private final Connection pgConn = Runner.run();
    private final Logger logger = LoggerFactory.getLogger(PgClientRepo.class);

    @Override
    public int createClient(ClientEntity client) {
        int resId;
        String sqlReq = """
                insert into clients (first_name, last_name, middle_name, phone, email, \
                gender_id, passport, driving_license, password) \
                values (?, ?, ?, ?, ?, ?, ?, ?, ?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setString(1, client.getFirstName());
            statement.setString(2, client.getLastName());
            statement.setString(3, client.getMiddleName());
            statement.setString(4, client.getPhone());
            statement.setString(5, client.getEmail());
            statement.setInt(6, client.getGenderId());
            statement.setString(7, client.getPassport());
            statement.setString(8, client.getDrivingLicense());
            statement.setString(9, client.getPassword());

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            logger.error("Unsuccessful client creation");
            throw new RuntimeException(e);
        }

        logger.info("Client was created");
        return resId;
    }

    @Override
    public void updateClient(ClientEntity client) {
        String sqlReq = """
                update clients set (first_name, last_name, middle_name, phone, email, \
                gender_id, passport, driving_license, password) \
                = (?, ?, ?, ?, ?, ?, ?, ?, ?)
                where id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setString(1, client.getFirstName());
            statement.setString(2, client.getLastName());
            statement.setString(3, client.getMiddleName());
            statement.setString(4, client.getPhone());
            statement.setString(5, client.getEmail());
            statement.setInt(6, client.getGenderId());
            statement.setString(7, client.getPassport());
            statement.setString(8, client.getDrivingLicense());
            statement.setString(9, client.getPassword());
            statement.setInt(10, client.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful client update");
            throw new RuntimeException(e);
        }

        logger.info("Client was updated");
    }

    @Override
    public void deleteClient(int id) {
        String sqlReq = """
                delete from clients
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful client deletion");
            throw new RuntimeException(e);
        }

        logger.info("Client was deleted");
    }

    @Override
    public ClientEntity getClient(int id) {
        ClientEntity client = null;

        String sqlReq = """
                select * from clients
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                client = new ClientEntity(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("middle_name"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getInt("gender_id"),
                        resultSet.getString("passport"),
                        resultSet.getString("driving_license"),
                        resultSet.getString("password"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return client;
    }

    @Override
    public ArrayList<ClientEntity> getClientList() {
        ArrayList<ClientEntity> clientList = new ArrayList<>();

        String sqlReq = """
                select * from clients
                order by id
                """;

        try (Statement statement = pgConn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlReq);

            while (resultSet.next()) {
                ClientEntity client = new ClientEntity(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("middle_name"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getInt("gender_id"),
                        resultSet.getString("passport"),
                        resultSet.getString("driving_license"),
                        resultSet.getString("password"));

                clientList.add(client);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return clientList;
    }

    @Override
    public ClientEntity logInClient(ClientEntity client) {
        ClientEntity logInClient = null;

        String sqlReq = """
                select * from clients
                where email = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setString(1, client.getEmail());

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                logInClient = new ClientEntity(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("middle_name"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getInt("gender_id"),
                        resultSet.getString("passport"),
                        resultSet.getString("driving_license"),
                        resultSet.getString("password"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if (logInClient == null) {
            return null;
        }

        if (client.getPassword().equals(logInClient.getPassword())) {
            return logInClient;
        } else {
            return null;
        }
    }
}
