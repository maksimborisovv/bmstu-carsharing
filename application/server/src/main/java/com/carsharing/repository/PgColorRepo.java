package com.carsharing.repository;

import com.carsharing.entity.ColorEntity;
import com.carsharing.service.IColorRepo;
import com.carsharing.starter.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class PgColorRepo implements IColorRepo {
    private final Connection pgConn = Runner.run();
    private final Logger logger = LoggerFactory.getLogger(PgColorRepo.class);

    @Override
    public int createColor(ColorEntity color) {
        int resId;
        String sqlReq = """
                insert into colors (name) \
                values (?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setString(1, color.getName());

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            logger.error("Unsuccessful color creation");
            throw new RuntimeException(e);
        }

        logger.info("Color was created");
        return resId;
    }

    @Override
    public void updateColor(ColorEntity color) {
        String sqlReq = """
                update colors set (name) \
                = (?)
                where id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setString(1, color.getName());
            statement.setInt(2, color.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful color update");
            throw new RuntimeException(e);
        }

        logger.info("Color was updated");
    }

    @Override
    public void deleteColor(int id) {
        String sqlReq = """
                delete from colors
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful color deletion");
            throw new RuntimeException(e);
        }

        logger.info("Color was deleted");
    }

    @Override
    public ColorEntity getColor(int id) {
        ColorEntity color = null;

        String sqlReq = """
                select * from colors
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                color = new ColorEntity(resultSet.getInt("id"),
                        resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return color;
    }

    @Override
    public ArrayList<ColorEntity> getColorList() {
        ArrayList<ColorEntity> colorList = new ArrayList<>();

        String sqlReq = """
                select * from colors
                order by id
                """;

        try (Statement statement = pgConn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlReq);

            while (resultSet.next()) {
                ColorEntity color = new ColorEntity(resultSet.getInt("id"),
                        resultSet.getString("name"));

                colorList.add(color);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return colorList;
    }
}
