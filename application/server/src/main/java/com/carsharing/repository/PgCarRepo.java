package com.carsharing.repository;

import com.carsharing.entity.CarEntity;
import com.carsharing.service.ICarRepo;
import com.carsharing.starter.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class PgCarRepo implements ICarRepo {
    private final Connection pgConn = Runner.run();
    private final Logger logger = LoggerFactory.getLogger(PgCarRepo.class);
    @Override
    public int createCar(CarEntity car) {
        int resId;
        String sqlReq = """
                insert into cars(model, plate_number, prod_year, color_id, \
                category_id, description, documents, owner_id) \
                values (?, ?, ?, ?, ?, ?, ?, ?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setString(1, car.getModel());
            statement.setString(2, car.getPlateNumber());
            statement.setInt(3, car.getProdYear());
            statement.setInt(4, car.getColorId());
            statement.setInt(5, car.getCategoryId());
            statement.setString(6, car.getDescription());
            statement.setString(7, car.getDocuments());
            statement.setInt(8, car.getOwnerId());

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            logger.error("Unsuccessful car creation");
            throw new RuntimeException(e);
        }

        logger.info("Car was created");
        return resId;
    }

    @Override
    public void updateCar(CarEntity car) {
        String sqlReq = """
                update cars set (model, plate_number, prod_year, color_id, \
                category_id, description, documents, owner_id) \
                = (?, ?, ?, ?, ?, ?, ?, ?)
                where id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setString(1, car.getModel());
            statement.setString(2, car.getPlateNumber());
            statement.setInt(3, car.getProdYear());
            statement.setInt(4, car.getColorId());
            statement.setInt(5, car.getCategoryId());
            statement.setString(6, car.getDescription());
            statement.setString(7, car.getDocuments());
            statement.setInt(8, car.getOwnerId());
            statement.setInt(9, car.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful car update");
            throw new RuntimeException(e);
        }

        logger.info("Car was updated");
    }

    @Override
    public void deleteCar(int id) {
        String sqlReq = """
                delete from cars
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful car deletion");
            throw new RuntimeException(e);
        }

        logger.info("Car was deleted");
    }

    @Override
    public CarEntity getCar(int id) {
        CarEntity car = null;

        String sqlReq = """
                select * from cars
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                car = new CarEntity(resultSet.getInt("id"),
                        resultSet.getString("model"),
                        resultSet.getString("plate_number"),
                        resultSet.getInt("prod_year"),
                        resultSet.getInt("color_id"),
                        resultSet.getInt("category_id"),
                        resultSet.getString("description"),
                        resultSet.getString("documents"),
                        resultSet.getInt("owner_id"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return car;
    }

    @Override
    public ArrayList<CarEntity> getCarList() {
        ArrayList<CarEntity> carList = new ArrayList<>();

        String sqlReq = """
                select * from cars
                order by id
                """;

        try (Statement statement = pgConn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlReq);

            while (resultSet.next()) {
                CarEntity car = new CarEntity(resultSet.getInt("id"),
                        resultSet.getString("model"),
                        resultSet.getString("plate_number"),
                        resultSet.getInt("prod_year"),
                        resultSet.getInt("color_id"),
                        resultSet.getInt("category_id"),
                        resultSet.getString("description"),
                        resultSet.getString("documents"),
                        resultSet.getInt("owner_id"));

                carList.add(car);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return carList;
    }

    @Override
    public ArrayList<CarEntity> getCarListByOwner(int ownerId) {
        ArrayList<CarEntity> carList = new ArrayList<>();

        String sqlReq = """
                select * from cars
                where owner_id = ?
                order by id
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, ownerId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                CarEntity car = new CarEntity(resultSet.getInt("id"),
                        resultSet.getString("model"),
                        resultSet.getString("plate_number"),
                        resultSet.getInt("prod_year"),
                        resultSet.getInt("color_id"),
                        resultSet.getInt("category_id"),
                        resultSet.getString("description"),
                        resultSet.getString("documents"),
                        resultSet.getInt("owner_id"));

                carList.add(car);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return carList;
    }
}
