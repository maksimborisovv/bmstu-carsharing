package com.carsharing.repository;

import com.carsharing.entity.OfferEntity;
import com.carsharing.service.IOfferRepo;
import com.carsharing.starter.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class PgOfferRepo implements IOfferRepo {
    private final Connection pgConn = Runner.run();
    private final Logger logger = LoggerFactory.getLogger(PgOfferRepo.class);

    @Override
    public int createOffer(OfferEntity offer) {
        int resId;
        String sqlReq = """
                insert into offers (car_id, owner_id, price, availability) \
                values (?, ?, ?, ?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setInt(1, offer.getCarId());
            statement.setInt(2, offer.getOwnerId());
            statement.setInt(3, offer.getPrice());
            statement.setBoolean(4, offer.getAvailability());

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            logger.error("Unsuccessful offer creation");
            throw new RuntimeException(e);
        }

        logger.info("Offer was created");
        return resId;
    }

    @Override
    public void updateOffer(OfferEntity offer) {
        String sqlReq = """
                update offers set (car_id, owner_id, price, availability) \
                = (?, ?, ?, ?)
                where id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, offer.getCarId());
            statement.setInt(2, offer.getOwnerId());
            statement.setInt(3, offer.getPrice());
            statement.setBoolean(4, offer.getAvailability());
            statement.setInt(5, offer.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful offer update");
            throw new RuntimeException(e);
        }

        logger.info("Offer was updated");
    }

    @Override
    public void deleteOffer(int id) {
        String sqlReq = """
                delete from offers
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful offer deletion");
            throw new RuntimeException(e);
        }

        logger.info("Offer was deleted");
    }

    @Override
    public OfferEntity getOffer(int id) {
        OfferEntity offer = null;

        String sqlReq = """
                select * from offers
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                offer = new OfferEntity(resultSet.getInt("id"),
                        resultSet.getInt("car_id"),
                        resultSet.getInt("owner_id"),
                        resultSet.getInt("price"),
                        resultSet.getBoolean("availability"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return offer;
    }

    @Override
    public ArrayList<OfferEntity> getOfferList() {
        ArrayList<OfferEntity> offerList = new ArrayList<>();

        String sqlReq = """
                select * from offers
                order by id
                """;

        try (Statement statement = pgConn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlReq);

            while (resultSet.next()) {
                OfferEntity offer = new OfferEntity(resultSet.getInt("id"),
                        resultSet.getInt("car_id"),
                        resultSet.getInt("owner_id"),
                        resultSet.getInt("price"),
                        resultSet.getBoolean("availability"));

                offerList.add(offer);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return offerList;
    }

    @Override
    public ArrayList<OfferEntity> getOfferListByOwner(int ownerId) {
        ArrayList<OfferEntity> offerList = new ArrayList<>();

        String sqlReq = """
                select * from offers
                where  owner_id = ?
                order by id
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, ownerId);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                OfferEntity offer = new OfferEntity(resultSet.getInt("id"),
                        resultSet.getInt("car_id"),
                        resultSet.getInt("owner_id"),
                        resultSet.getInt("price"),
                        resultSet.getBoolean("availability"));

                offerList.add(offer);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return offerList;
    }
}
