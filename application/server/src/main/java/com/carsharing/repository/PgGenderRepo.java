package com.carsharing.repository;

import com.carsharing.entity.GenderEntity;
import com.carsharing.service.IGenderRepo;
import com.carsharing.starter.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class PgGenderRepo implements IGenderRepo {
    private final Connection pgConn = Runner.run();
    private final Logger logger = LoggerFactory.getLogger(PgGenderRepo.class);

    @Override
    public int createGender(GenderEntity gender) {
        int resId;
        String sqlReq = """
                insert into genders (name) \
                values (?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setString(1, gender.getName());

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            logger.error("Unsuccessful gender creation");
            throw new RuntimeException(e);
        }

        logger.info("Gender was created");
        return resId;
    }

    @Override
    public void updateGender(GenderEntity gender) {
        String sqlReq = """
                update genders set (name) \
                = (?)
                where id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setString(1, gender.getName());
            statement.setInt(2, gender.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful gender update");
            throw new RuntimeException(e);
        }

        logger.info("Gender was updated");
    }

    @Override
    public void deleteGender(int id) {
        String sqlReq = """
                delete from genders
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful gender deletion");
            throw new RuntimeException(e);
        }

        logger.info("Gender was deleted");
    }

    @Override
    public GenderEntity getGender(int id) {
        GenderEntity gender = null;

        String sqlReq = """
                select * from genders
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                gender = new GenderEntity(resultSet.getInt("id"),
                        resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return gender;
    }

    @Override
    public ArrayList<GenderEntity> getGenderList() {
        ArrayList<GenderEntity> genderList = new ArrayList<>();

        String sqlReq = """
                select * from genders
                order by id
                """;

        try (Statement statement = pgConn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlReq);

            while (resultSet.next()) {
                GenderEntity category = new GenderEntity(resultSet.getInt("id"),
                        resultSet.getString("name"));

                genderList.add(category);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return genderList;
    }
}
