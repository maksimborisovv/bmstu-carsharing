package com.carsharing.repository;

import com.carsharing.entity.OrderEntity;
import com.carsharing.service.IOrderRepo;
import com.carsharing.starter.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Repository
public class PgOrderRepo implements IOrderRepo {
    private final Connection pgConn = Runner.run();
    private final Logger logger = LoggerFactory.getLogger(PgOrderRepo.class);

    @Override
    public int createOrder(OrderEntity order) {
        int resId;
        String sqlReq = """
                insert into orders (offer_id, client_id, rental_begin, rental_end) \
                values (?, ?, ?, ?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setInt(1, order.getOfferId());
            statement.setInt(2, order.getClientId());
            statement.setTimestamp(3, Timestamp.valueOf(order.getRentalBegin()));
            statement.setTimestamp(4, Timestamp.valueOf(order.getRentalEnd()));

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            logger.error("Unsuccessful order creation");
            throw new RuntimeException(e);
        }

        logger.info("Order was created");
        return resId;
    }

    @Override
    public void updateOrder(OrderEntity order) {
        String sqlReq = """
                update orders set (offer_id, client_id, rental_begin, rental_end) \
                = (?, ?, ?, ?)
                where id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, order.getOfferId());
            statement.setInt(2, order.getClientId());
            statement.setTimestamp(3, Timestamp.valueOf(order.getRentalBegin()));
            statement.setTimestamp(4, Timestamp.valueOf(order.getRentalEnd()));
            statement.setInt(5, order.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful order update");
            throw new RuntimeException(e);
        }

        logger.info("Order was updated");
    }

    @Override
    public void deleteOrder(int id) {
        String sqlReq = """
                delete from orders
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful order deletion");
            throw new RuntimeException(e);
        }

        logger.info("Order was deleted");
    }

    @Override
    public OrderEntity getOrder(int id) {
        OrderEntity order = null;

        String sqlReq = """
                select * from orders
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                order = new OrderEntity(resultSet.getInt("id"),
                        resultSet.getInt("offer_id"),
                        resultSet.getInt("client_id"),
                        resultSet.getTimestamp("rental_begin").toLocalDateTime(),
                        null);
                if (resultSet.getTimestamp("rental_end") != null) {
                    order.setRentalEnd(resultSet.getTimestamp("rental_end").toLocalDateTime());
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return order;
    }

    @Override
    public ArrayList<OrderEntity> getOrderList() {
        ArrayList<OrderEntity> orderList = new ArrayList<>();

        String sqlReq = """
                select * from orders
                order by id
                """;

        try (Statement statement = pgConn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlReq);

            while (resultSet.next()) {
                OrderEntity order = new OrderEntity(resultSet.getInt("id"),
                        resultSet.getInt("offer_id"),
                        resultSet.getInt("client_id"),
                        resultSet.getTimestamp("rental_begin").toLocalDateTime(),
                        resultSet.getTimestamp("rental_end").toLocalDateTime());

                orderList.add(order);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return orderList;
    }

    @Override
    public OrderEntity startRenting(OrderEntity order) {
        int resId;
        String sqlReq = """
                insert into orders (offer_id, client_id, rental_begin) \
                values (?, ?, ?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setInt(1, order.getOfferId());
            statement.setInt(2, order.getClientId());
            statement.setTimestamp(3, Timestamp.valueOf(order.getRentalBegin()));

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        order.setId(resId);

        sqlReq = """
        update offers set availability = false where id = ?;
        """;
        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, order.getOfferId());

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return order;
    }

    @Override
    public void endRenting(int offerId, LocalDateTime rentalEnd) {
        String sqlReq = """
                update orders set rental_end \
                = ?
                where offer_id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setTimestamp(1, Timestamp.valueOf(rentalEnd));
            statement.setInt(2, offerId);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful order update");
            throw new RuntimeException(e);
        }

        sqlReq = """
        update offers set availability = true where id = ?;
        """;
        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, offerId);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ArrayList<OrderEntity> getOrdersByClient(int clientId) {
        ArrayList<OrderEntity> orderList = new ArrayList<>();

        String sqlReq = """
                select * from orders
                where client_id = ?
                order by id
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, clientId);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                OrderEntity order = new OrderEntity(resultSet.getInt("id"),
                        resultSet.getInt("offer_id"),
                        resultSet.getInt("client_id"),
                        resultSet.getTimestamp("rental_begin").toLocalDateTime(),
                        resultSet.getTimestamp("rental_end").toLocalDateTime());

                orderList.add(order);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return orderList;
    }
}
