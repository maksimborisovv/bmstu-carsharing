package com.carsharing.repository;

import com.carsharing.entity.CategoryEntity;
import com.carsharing.service.ICategoryRepo;
import com.carsharing.starter.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class PgCategoryRepo implements ICategoryRepo {
    private final Connection pgConn = Runner.run();
    private final Logger logger = LoggerFactory.getLogger(PgCategoryRepo.class);

    @Override
    public int createCategory(CategoryEntity category) {
        int resId;
        String sqlReq = """
                insert into categories (name) \
                values (?);
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq, new String[] {"id"})) {
            statement.setString(1, category.getName());

            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            resId = keys.getInt("id");
        } catch (SQLException e) {
            logger.error("Unsuccessful category creation");
            throw new RuntimeException(e);
        }

        logger.info("Category was created");
        return resId;
    }

    @Override
    public void updateCategory(CategoryEntity category) {
        String sqlReq = """
                update categories set (name) \
                = (?)
                where id = ?;
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setString(1, category.getName());
            statement.setInt(2, category.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful category update");
            throw new RuntimeException(e);
        }

        logger.info("Category was updated");
    }

    @Override
    public void deleteCategory(int id) {
        String sqlReq = """
                delete from categories
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Unsuccessful category deletion");
            throw new RuntimeException(e);
        }

        logger.info("Category was deleted");
    }

    @Override
    public CategoryEntity getCategory(int id) {
        CategoryEntity category = null;

        String sqlReq = """
                select * from categories
                where id = ?
                """;

        try (PreparedStatement statement = pgConn.prepareStatement(sqlReq)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                category = new CategoryEntity(resultSet.getInt("id"),
                        resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return category;
    }

    @Override
    public ArrayList<CategoryEntity> getCategoryList() {
        ArrayList<CategoryEntity> categoryList = new ArrayList<>();

        String sqlReq = """
                select * from categories
                order by id
                """;

        try (Statement statement = pgConn.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlReq);

            while (resultSet.next()) {
                CategoryEntity category = new CategoryEntity(resultSet.getInt("id"),
                        resultSet.getString("name"));

                categoryList.add(category);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return categoryList;
    }
}
