package com.carsharing.controller;

import com.carsharing.dto.ColorDTORequest;
import com.carsharing.dto.ColorDTOResponse;
import com.carsharing.entity.ColorEntity;
import com.carsharing.mapper.ColorMapper;
import com.carsharing.service.ColorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.carsharing.config.SwaggerConfig.COLOR_TAG;

@Tag(name = COLOR_TAG)
@RestController
@RequestMapping("/colors")
public class ColorController {
    private final ColorService service;
    private final ColorMapper mapper;

    public ColorController(ColorService service, ColorMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Create color")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createColor(@RequestBody ColorDTORequest color) {
        return service.createColor(mapper.toEntity(color));
    }

    @Operation(summary = "Update color")
    @PutMapping("/{id:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateColor(@RequestBody ColorDTORequest color,
                            @PathVariable int id) {
        service.updateColor(mapper.toEntity(color), id);
    }

    @Operation(summary = "Delete color")
    @DeleteMapping("/{id:\\d+}")
    public void deleteColor(@PathVariable int id) {
        service.deleteColor(id);
    }

    @Operation(summary = "Get color")
    @GetMapping("/{id:\\d+}")
    public ColorDTOResponse getColor(@PathVariable int id) {
        return mapper.toDTOResponse(service.getColor(id));
    }

    @Operation(summary = "Get color list")
    @GetMapping
    public ArrayList<ColorDTOResponse> getColorList() {
        ArrayList<ColorEntity> colorEntities;
        colorEntities = service.getColorList();

        ArrayList<ColorDTOResponse> colorDTOResponses = new ArrayList<>();
        for (var entity : colorEntities) {
            colorDTOResponses.add(mapper.toDTOResponse(entity));
        }

        return colorDTOResponses;
    }
}
