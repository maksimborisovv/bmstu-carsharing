package com.carsharing.controller;

import com.carsharing.dto.CarDTORequest;
import com.carsharing.dto.CarDTOResponse;
import com.carsharing.entity.CarEntity;
import com.carsharing.mapper.CarMapper;
import com.carsharing.service.CarService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.carsharing.config.SwaggerConfig.CAR_TAG;

@Tag(name = CAR_TAG)
@RestController
@RequestMapping("/cars")
public class CarController {
    private final CarService service;
    private final CarMapper mapper;

    public CarController(CarService service, CarMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Create car")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createCar(@RequestBody CarDTORequest car) {
        return service.createCar(mapper.toEntity(car));
    }

    @Operation(summary = "Update car")
    @PutMapping("/{id:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCar(@RequestBody CarDTORequest car,
                          @PathVariable int id) {
        service.updateCar(mapper.toEntity(car), id);
    }

    @Operation(summary = "Delete car")
    @DeleteMapping("/{id:\\d+}")
    public void deleteCar(@PathVariable int id) {
        service.deleteCar(id);
    }

    @Operation(summary = "Get car")
    @GetMapping("/{id:\\d+}")
    public CarDTOResponse getCar(@PathVariable int id) {
        return mapper.toDTOResponse(service.getCar(id));
    }

    @Operation(summary = "Get car list")
    @GetMapping
    public ArrayList<CarDTOResponse> getCarList(
            @RequestParam(value = "owner", required = false) Integer ownerId
    ) {
        ArrayList<CarEntity> carEntities;
        if (ownerId == null) {
            carEntities = service.getCarList();
        } else {
            carEntities = service.getCarListByOwner(ownerId);
        }
        ArrayList<CarDTOResponse> carDTOResponses = new ArrayList<>();
        for (var entity : carEntities) {
            carDTOResponses.add(mapper.toDTOResponse(entity));
        }

        return carDTOResponses;
    }
}
