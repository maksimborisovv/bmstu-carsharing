package com.carsharing.controller;

import com.carsharing.dto.GenderDTORequest;
import com.carsharing.dto.GenderDTOResponse;
import com.carsharing.entity.GenderEntity;
import com.carsharing.mapper.GenderMapper;
import com.carsharing.service.GenderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.carsharing.config.SwaggerConfig.GENDER_TAG;

@Tag(name = GENDER_TAG)
@RestController
@RequestMapping("/genders")
public class GenderController {
    private final GenderService service;
    private final GenderMapper mapper;

    public GenderController(GenderService service, GenderMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Create gender")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createGender(@RequestBody GenderDTORequest gender) {
        return service.createGender(mapper.toEntity(gender));
    }

    @Operation(summary = "Update gender")
    @PutMapping("/{id:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateGender(@RequestBody GenderDTORequest gender,
                             @PathVariable int id) {
        service.updateGender(mapper.toEntity(gender), id);
    }

    @Operation(summary = "Delete gender")
    @DeleteMapping("/{id:\\d+}")
    public void deleteGender(@PathVariable int id) {
        service.deleteGender(id);
    }

    @Operation(summary = "Get gender")
    @GetMapping("/{id:\\d+}")
    public GenderDTOResponse getGender(@PathVariable int id) {
        return mapper.toDTOResponse(service.getGender(id));
    }

    @Operation(summary = "Get gender list")
    @GetMapping
    public ArrayList<GenderDTOResponse> getGenderList() {
        ArrayList<GenderEntity> genderEntities;
        genderEntities = service.getGenderList();

        ArrayList<GenderDTOResponse> genderDTOResponses = new ArrayList<>();
        for (var entity : genderEntities) {
            genderDTOResponses.add(mapper.toDTOResponse(entity));
        }

        return genderDTOResponses;
    }
}
