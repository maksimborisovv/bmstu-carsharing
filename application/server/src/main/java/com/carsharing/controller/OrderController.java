package com.carsharing.controller;

import com.carsharing.dto.OrderDTORentalEnd;
import com.carsharing.dto.OrderDTORequest;
import com.carsharing.dto.OrderDTOResponse;
import com.carsharing.entity.OrderEntity;
import com.carsharing.mapper.OrderMapper;
import com.carsharing.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.carsharing.config.SwaggerConfig.ORDER_TAG;

@Tag(name = ORDER_TAG)
@RestController
@RequestMapping("/orders")
public class OrderController {
    private final OrderService service;
    private final OrderMapper mapper;

    public OrderController(OrderService service, OrderMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Create order")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createOrder(@RequestBody OrderDTORequest order) {
        return service.createOrder(mapper.toEntity(order));
    }

    @Operation(summary = "Update order")
    @PutMapping("/{id:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOrder(@RequestBody OrderDTORequest order,
                            @PathVariable int id) {
        service.updateOrder(mapper.toEntity(order), id);
    }

    @Operation(summary = "Delete order")
    @DeleteMapping("/{id:\\d+}")
    public void deleteOrder(@PathVariable int id) {
        service.deleteOrder(id);
    }

    @Operation(summary = "Get order")
    @GetMapping("/{id:\\d+}")
    public OrderDTOResponse getOrder(@PathVariable int id) {
        return mapper.toDTOResponse(service.getOrder(id));
    }

    @Operation(summary = "Get order list")
    @GetMapping
    public ArrayList<OrderDTOResponse> getOrderList(
            @RequestParam(name = "client", required = false) Integer clientId) {
        ArrayList<OrderEntity> orderEntities;
        if (clientId == null) {
            orderEntities = service.getOrderList();
        } else {
            orderEntities = service.getOrderByClient(clientId);
        }

        ArrayList<OrderDTOResponse> orderDTOResponses = new ArrayList<>();
        for (var entity : orderEntities) {
            orderDTOResponses.add(mapper.toDTOResponse(entity));
        }

        return orderDTOResponses;
    }

    @Operation(summary = "Start renting")
    @PostMapping("/start")
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDTOResponse startRenting(@RequestBody OrderDTORequest order) {
        return mapper.toDTOResponse(service.startRenting(mapper.toEntity(order)));
    }

    @Operation(summary = "End renting")
    @PutMapping("/end/{offerId:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void endRenting(@PathVariable int offerId, @RequestBody OrderDTORentalEnd rentalEndDTO) {
        service.endRenting(offerId, rentalEndDTO.rentalEnd);
    }
}
