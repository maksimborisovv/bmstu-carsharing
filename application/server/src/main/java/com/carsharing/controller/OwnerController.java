package com.carsharing.controller;

import com.carsharing.dto.OwnerDTORequest;
import com.carsharing.dto.OwnerDTOResponse;
import com.carsharing.entity.OwnerEntity;
import com.carsharing.mapper.OwnerMapper;
import com.carsharing.service.OwnerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.carsharing.config.SwaggerConfig.OWNER_TAG;

@Tag(name = OWNER_TAG)
@RestController
@RequestMapping("/owners")
public class OwnerController {
    private final OwnerService service;
    private final OwnerMapper mapper;

    public OwnerController(OwnerService service, OwnerMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Create owner")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createOwner(@RequestBody OwnerDTORequest owner) {
        return service.createOwner(mapper.toEntity(owner));
    }

    @Operation(summary = "Update owner")
    @PutMapping("/{id:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOwner(@RequestBody OwnerDTORequest owner,
                            @PathVariable int id) {
        service.updateOwner(mapper.toEntity(owner), id);
    }

    @Operation(summary = "Delete owner")
    @DeleteMapping("/{id:\\d+}")
    public void deleteOwner(@PathVariable int id) {
        service.deleteOwner(id);
    }

    @Operation(summary = "Get owner")
    @GetMapping("/{id:\\d+}")
    public OwnerDTOResponse getOwner(@PathVariable int id) {
        return mapper.toDTOResponse(service.getOwner(id));
    }

    @Operation(summary = "Get owner list")
    @GetMapping
    public ArrayList<OwnerDTOResponse> getOwnerList() {
        ArrayList<OwnerEntity> ownerEntities;
        ownerEntities = service.getOwnerList();

        ArrayList<OwnerDTOResponse> ownerDTOResponses = new ArrayList<>();
        for (var entity : ownerEntities) {
            ownerDTOResponses.add(mapper.toDTOResponse(entity));
        }

        return ownerDTOResponses;
    }

    public OwnerEntity logInOwner(OwnerEntity owner) {
        return service.logInOwner(owner);
    }
}
