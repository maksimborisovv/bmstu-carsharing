package com.carsharing.controller;

import com.carsharing.dto.ClientDTORequest;
import com.carsharing.dto.ClientDTOResponse;
import com.carsharing.entity.ClientEntity;
import com.carsharing.mapper.ClientMapper;
import com.carsharing.service.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.carsharing.config.SwaggerConfig.CLIENT_TAG;

@Tag(name = CLIENT_TAG)
@RestController
@RequestMapping("/clients")
public class ClientController {
    private final ClientService service;
    private final ClientMapper mapper;

    public ClientController(ClientService service, ClientMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Create client")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createClient(@RequestBody ClientDTORequest client) {
        return service.createClient(mapper.toEntity(client));
    }

    public ClientEntity logInClient(ClientEntity client) {
        return service.logInClient(client);
    }

    @Operation(summary = "Update client")
    @PutMapping("/{id:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateClient(@RequestBody ClientDTORequest client,
                             @PathVariable int id) {
        service.updateClient(mapper.toEntity(client), id);
    }

    @Operation(summary = "Delete client")
    @DeleteMapping("/{id:\\d+}")
    public void deleteClient(@PathVariable int id) {
        service.deleteClient(id);
    }

    @Operation(summary = "Get client")
    @GetMapping("/{id:\\d+}")
    public ClientDTOResponse getClient(@PathVariable int id) {
        return mapper.toDTOResponse(service.getClient(id));
    }

    @Operation(summary = "Get client list")
    @GetMapping
    public ArrayList<ClientDTOResponse> getClientList() {
        ArrayList<ClientEntity> clientEntities;
        clientEntities = service.getClientList();

        ArrayList<ClientDTOResponse> clientDTOResponses = new ArrayList<>();
        for (var entity : clientEntities) {
            clientDTOResponses.add(mapper.toDTOResponse(entity));
        }

        return clientDTOResponses;
    }
}
