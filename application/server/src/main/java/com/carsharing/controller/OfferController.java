package com.carsharing.controller;

import com.carsharing.dto.OfferDTORequest;
import com.carsharing.dto.OfferDTOResponse;
import com.carsharing.entity.OfferEntity;
import com.carsharing.mapper.OfferMapper;
import com.carsharing.service.OfferService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.carsharing.config.SwaggerConfig.OFFER_TAG;

@Tag(name = OFFER_TAG)
@RestController
@RequestMapping("/offers")
public class OfferController {
    private final OfferService service;
    private final OfferMapper mapper;

    public OfferController(OfferService service, OfferMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Create offer")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createOffer(@RequestBody OfferDTORequest offer) {
        return service.createOffer(mapper.toEntity(offer));
    }

    @Operation(summary = "Update offer")
    @PutMapping("/{id:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOffer(@RequestBody OfferDTORequest offer,
                            @PathVariable int id) {
        service.updateOffer(mapper.toEntity(offer), id);
    }

    @Operation(summary = "Delete offer")
    @DeleteMapping("/{id:\\d+}")
    public void deleteOffer(@PathVariable int id) {
        service.deleteOffer(id);
    }

    @Operation(summary = "Get offer")
    @GetMapping("/{id:\\d+}")
    public OfferDTOResponse getOffer(@PathVariable int id) {
        return mapper.toDTOResponse(service.getOffer(id));
    }

    @Operation(summary = "Get offer list")
    @GetMapping
    public ArrayList<OfferDTOResponse> getOfferList(
            @RequestParam(name = "owner", required = false) Integer ownerId) {
        ArrayList<OfferEntity> offerEntities;
        if (ownerId == null) {
            offerEntities = service.getOfferList();
        } else {
            offerEntities = service.getOfferListByOwner(ownerId);
        }

        ArrayList<OfferDTOResponse> offerDTOResponses = new ArrayList<>();
        for (var entity: offerEntities) {
            offerDTOResponses.add(mapper.toDTOResponse(entity));
        }

        return offerDTOResponses;
    }
}
