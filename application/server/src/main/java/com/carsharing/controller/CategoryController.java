package com.carsharing.controller;

import com.carsharing.dto.CategoryDTORequest;
import com.carsharing.dto.CategoryDTOResponse;
import com.carsharing.entity.CategoryEntity;
import com.carsharing.mapper.CategoryMapper;
import com.carsharing.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.carsharing.config.SwaggerConfig.CATEGORY_TAG;

@Tag(name = CATEGORY_TAG)
@RestController
@RequestMapping("/categories")
public class CategoryController {
    private final CategoryService service;
    private final CategoryMapper mapper;

    public CategoryController(CategoryService service, CategoryMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Create category")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int createCategory(@RequestBody CategoryDTORequest category) {
        return service.createCategory(mapper.toEntity(category));
    }

    @Operation(summary = "Update category")
    @PutMapping("/{id:\\d+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCategory(@RequestBody CategoryDTORequest category,
                               @PathVariable int id) {
        service.updateCategory(mapper.toEntity(category), id);
    }

    @Operation(summary = "Delete category")
    @DeleteMapping("/{id:\\d+}")
    public void deleteCategory(@PathVariable int id) {
        service.deleteCategory(id);
    }

    @Operation(summary = "Get catogory")
    @GetMapping("/{id:\\d+}")
    public CategoryDTOResponse getCategory(@PathVariable int id) {
        return mapper.toDTOResponse(service.getCategory(id));
    }

    @Operation(summary = "Get category list")
    @GetMapping
    public ArrayList<CategoryDTOResponse> getCategoryList() {
        ArrayList<CategoryEntity> categoryEntities;
        categoryEntities = service.getCategoryList();

        ArrayList<CategoryDTOResponse> categoryDTOResponses = new ArrayList<>();
        for (var entity : categoryEntities) {
            categoryDTOResponses.add(mapper.toDTOResponse(entity));
        }

        return categoryDTOResponses;
    }
}
