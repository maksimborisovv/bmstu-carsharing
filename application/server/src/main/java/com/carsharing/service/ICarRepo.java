package com.carsharing.service;

import com.carsharing.entity.CarEntity;

import java.util.ArrayList;

public interface ICarRepo {
    int createCar(CarEntity car);
    void updateCar(CarEntity car);
    void deleteCar(int id);
    CarEntity getCar(int id);
    ArrayList<CarEntity> getCarList();

    ArrayList<CarEntity> getCarListByOwner(int ownerId);
}
