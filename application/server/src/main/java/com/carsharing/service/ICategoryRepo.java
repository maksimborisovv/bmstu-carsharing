package com.carsharing.service;

import com.carsharing.entity.CategoryEntity;

import java.util.ArrayList;

public interface ICategoryRepo {
    int createCategory(CategoryEntity category);
    void updateCategory(CategoryEntity category);
    void deleteCategory(int id);
    CategoryEntity getCategory(int id);
    ArrayList<CategoryEntity> getCategoryList();
}
