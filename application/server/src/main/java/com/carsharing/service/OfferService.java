package com.carsharing.service;

import com.carsharing.entity.OfferEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class OfferService {
    private final IOfferRepo repo;

    public OfferService(IOfferRepo repo) {
        this.repo = repo;
    }

    public int createOffer(OfferEntity offer) {
        return repo.createOffer(offer);
    }

    public void updateOffer(OfferEntity offer, int id) {
        offer.setId(id);
        repo.updateOffer(offer);
    }

    public void deleteOffer(int id) {
        repo.deleteOffer(id);
    }

    public OfferEntity getOffer(int id) {
        return repo.getOffer(id);
    }

    public ArrayList<OfferEntity> getOfferList() {
        return repo.getOfferList();
    }

    public ArrayList<OfferEntity> getOfferListByOwner(int ownerId) {
        return repo.getOfferListByOwner(ownerId);
    }
}
