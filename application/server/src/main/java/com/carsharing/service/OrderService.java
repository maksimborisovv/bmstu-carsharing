package com.carsharing.service;

import com.carsharing.entity.OrderEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class OrderService {
    private final IOrderRepo repo;

    public OrderService(IOrderRepo repo) {
        this.repo = repo;
    }

    public int createOrder(OrderEntity order) {
        return repo.createOrder(order);
    }

    public void updateOrder(OrderEntity order, int id) {
        order.setId(id);
        repo.updateOrder(order);
    }

    public void deleteOrder(int id) {
        repo.deleteOrder(id);
    }

    public OrderEntity getOrder(int id) {
        return repo.getOrder(id);
    }

    public ArrayList<OrderEntity> getOrderList() {
        return repo.getOrderList();
    }

    public OrderEntity startRenting(OrderEntity order) {
        return repo.startRenting(order);
    }

    public void endRenting(int offerId, LocalDateTime rentalEnd) {
        repo.endRenting(offerId, rentalEnd);
    }

    public ArrayList<OrderEntity> getOrderByClient(int clientId) {
        return repo.getOrdersByClient(clientId);
    }
}
