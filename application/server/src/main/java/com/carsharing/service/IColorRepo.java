package com.carsharing.service;

import com.carsharing.entity.ColorEntity;

import java.util.ArrayList;

public interface IColorRepo {
    int createColor(ColorEntity color);
    void updateColor(ColorEntity color);
    void deleteColor(int id);
    ColorEntity getColor(int id);
    ArrayList<ColorEntity> getColorList();
}
