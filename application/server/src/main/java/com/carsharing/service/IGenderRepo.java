package com.carsharing.service;

import com.carsharing.entity.GenderEntity;

import java.util.ArrayList;

public interface IGenderRepo {
    int createGender(GenderEntity gender);
    void updateGender(GenderEntity gender);
    void deleteGender(int id);
    GenderEntity getGender(int id);
    ArrayList<GenderEntity> getGenderList();
}
