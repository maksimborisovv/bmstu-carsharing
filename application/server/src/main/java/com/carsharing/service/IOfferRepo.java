package com.carsharing.service;

import com.carsharing.entity.OfferEntity;

import java.util.ArrayList;

public interface IOfferRepo {
    int createOffer(OfferEntity offer);
    void updateOffer(OfferEntity offer);
    void deleteOffer(int id);
    OfferEntity getOffer(int id);
    ArrayList<OfferEntity> getOfferList();

    ArrayList<OfferEntity> getOfferListByOwner(int ownerId);
}
