package com.carsharing.service;

import com.carsharing.entity.OrderEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;

public interface IOrderRepo {
    int createOrder(OrderEntity order);
    void updateOrder(OrderEntity order);
    void deleteOrder(int id);
    OrderEntity getOrder(int id);
    ArrayList<OrderEntity> getOrderList();
    OrderEntity startRenting(OrderEntity order);

    void endRenting(int offerId, LocalDateTime rentalEnd);

    ArrayList<OrderEntity> getOrdersByClient(int clientId);
}
