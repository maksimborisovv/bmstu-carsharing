package com.carsharing.service;

import com.carsharing.entity.CarEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CarService {
    private final ICarRepo repo;

    public CarService(ICarRepo repo) {
        this.repo = repo;
    }

    public int createCar(CarEntity car) {
        return repo.createCar(car);
    }

    public void updateCar(CarEntity car, int id) {
        car.setId(id);
        repo.updateCar(car);
    }

    public void deleteCar(int id) {
        repo.deleteCar(id);
    }

    public CarEntity getCar(int id) {
        return repo.getCar(id);
    }

    public ArrayList<CarEntity> getCarList() {
        return repo.getCarList();
    }

    public ArrayList<CarEntity> getCarListByOwner(int ownerId) {
        return repo.getCarListByOwner(ownerId);
    }
}
