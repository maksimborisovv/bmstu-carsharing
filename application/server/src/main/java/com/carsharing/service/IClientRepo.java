package com.carsharing.service;

import com.carsharing.entity.ClientEntity;

import java.util.ArrayList;

public interface IClientRepo {
    int createClient(ClientEntity client);
    void updateClient(ClientEntity client);
    void deleteClient(int id);
    ClientEntity getClient(int id);
    ArrayList<ClientEntity> getClientList();

    ClientEntity logInClient(ClientEntity client);
}
