package com.carsharing.service;

import com.carsharing.entity.ColorEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ColorService {
    private final IColorRepo repo;

    public ColorService(IColorRepo repo) {
        this.repo = repo;
    }

    public int createColor(ColorEntity color) {
        return repo.createColor(color);
    }

    public void updateColor(ColorEntity color, int id) {
        color.setId(id);
        repo.updateColor(color);
    }

    public void deleteColor(int id) {
        repo.deleteColor(id);
    }

    public ColorEntity getColor(int id) {
        return repo.getColor(id);
    }

    public ArrayList<ColorEntity> getColorList() {
        return repo.getColorList();
    }
}
