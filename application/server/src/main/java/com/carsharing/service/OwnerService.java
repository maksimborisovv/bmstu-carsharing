package com.carsharing.service;

import com.carsharing.entity.OwnerEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.ArrayList;

@Service
public class OwnerService {
    private final IOwnerRepo repo;

    public OwnerService(IOwnerRepo repo) {
        this.repo = repo;
    }

    private String toHash(String data) {
        return DigestUtils.md5DigestAsHex(data.getBytes());
    }

    public int createOwner(OwnerEntity owner) {
        owner.setPassword(toHash(owner.getPassword()));
        return repo.createOwner(owner);
    }

    public void updateOwner(OwnerEntity owner, int id) {
        owner.setId(id);
        repo.updateOwner(owner);
    }

    public void deleteOwner(int id) {
        repo.deleteOwner(id);
    }

    public OwnerEntity getOwner(int id) {
        return repo.getOwner(id);
    }

    public ArrayList<OwnerEntity> getOwnerList() {
        return repo.getOwnerList();
    }

    public OwnerEntity logInOwner(OwnerEntity owner) {
        owner.setPassword(toHash(owner.getPassword()));
        return repo.logInOwner(owner);
    }
}
