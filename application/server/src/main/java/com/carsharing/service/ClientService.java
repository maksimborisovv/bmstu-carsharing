package com.carsharing.service;

import com.carsharing.entity.ClientEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.ArrayList;

@Service
public class ClientService {
    private final IClientRepo repo;

    public ClientService(IClientRepo repo) {
        this.repo = repo;
    }

    private String toHash(String data) {
        return DigestUtils.md5DigestAsHex(data.getBytes());
    }

    public int createClient(ClientEntity client) {
        client.setPassword(toHash(client.getPassword()));
        return repo.createClient(client);
    }

    public void updateClient(ClientEntity client, int id) {
        client.setId(id);
        repo.updateClient(client);
    }

    public void deleteClient(int id) {
        repo.deleteClient(id);
    }

    public ClientEntity getClient(int id) {
        return repo.getClient(id);
    }

    public ArrayList<ClientEntity> getClientList() {
        return repo.getClientList();
    }

    public ClientEntity logInClient(ClientEntity client) {
        client.setPassword(toHash(client.getPassword()));
        return repo.logInClient(client);
    }
}
