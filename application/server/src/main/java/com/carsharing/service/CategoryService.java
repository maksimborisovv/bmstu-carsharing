package com.carsharing.service;

import com.carsharing.entity.CategoryEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CategoryService {
    private final ICategoryRepo repo;

    public CategoryService(ICategoryRepo repo) {
        this.repo = repo;
    }

    public int createCategory(CategoryEntity category) {
        return repo.createCategory(category);
    }

    public void updateCategory(CategoryEntity category, int id) {
        category.setId(id);
        repo.updateCategory(category);
    }

    public void deleteCategory(int id) {
        repo.deleteCategory(id);
    }

    public CategoryEntity getCategory(int id) {
        return repo.getCategory(id);
    }

    public ArrayList<CategoryEntity> getCategoryList() {
        return repo.getCategoryList();
    }
}
