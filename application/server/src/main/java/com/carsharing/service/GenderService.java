package com.carsharing.service;

import com.carsharing.entity.GenderEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class GenderService {
    final private IGenderRepo repo;

    public GenderService(IGenderRepo repo) {
        this.repo = repo;
    }

    public int createGender(GenderEntity gender) {
        return repo.createGender(gender);
    }

    public void updateGender(GenderEntity gender, int id) {
        gender.setId(id);
        repo.updateGender(gender);
    }

    public void deleteGender(int id) {
        repo.deleteGender(id);
    }

    public GenderEntity getGender(int id) {
        return repo.getGender(id);
    }

    public ArrayList<GenderEntity> getGenderList() {
        return repo.getGenderList();
    }
}
