package com.carsharing.service;

import com.carsharing.entity.OwnerEntity;

import java.util.ArrayList;

public interface IOwnerRepo {
    int createOwner(OwnerEntity owner);
    void updateOwner(OwnerEntity owner);
    void deleteOwner(int id);
    OwnerEntity getOwner(int id);
    ArrayList<OwnerEntity> getOwnerList();

    OwnerEntity logInOwner(OwnerEntity owner);
}
