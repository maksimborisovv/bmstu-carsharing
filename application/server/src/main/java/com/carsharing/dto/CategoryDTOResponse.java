package com.carsharing.dto;

public record CategoryDTOResponse(
        int id,
        String name
) {
}
