package com.carsharing.dto;

public record GenderDTOResponse(
        int id,
        String name
) {
}
