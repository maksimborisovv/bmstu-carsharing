package com.carsharing.dto;

public record ClientDTOResponse(
        int id,
        String firstName,
        String lastName,
        String middleName,
        String phone,
        String email,
        int genderId,
        String passport,
        String drivingLicense,
        String password
) {
}
