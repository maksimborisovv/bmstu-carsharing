package com.carsharing.dto;

import java.time.LocalDateTime;

public record OrderDTORequest(
        int offerId,
        int clientId,
        LocalDateTime rentalBegin,
        LocalDateTime rentalEnd
) {
}
