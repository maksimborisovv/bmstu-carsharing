package com.carsharing.dto;

public record OfferDTOResponse(
        int id,
        int carId,
        int ownerId,
        int price,
        boolean availability
) {
}
