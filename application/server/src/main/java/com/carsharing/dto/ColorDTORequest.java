package com.carsharing.dto;

public record ColorDTORequest(
        String name
) {
}
