package com.carsharing.dto;

import java.time.LocalDateTime;

public record OrderDTOResponse(
        int id,
        int offerId,
        int clientId,
        LocalDateTime rentalBegin,
        LocalDateTime rentalEnd
) {
}
