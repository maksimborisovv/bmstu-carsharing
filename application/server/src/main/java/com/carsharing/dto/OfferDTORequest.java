package com.carsharing.dto;

public record OfferDTORequest(
        int carId,
        int ownerId,
        int price,
        boolean availability
) {
}
