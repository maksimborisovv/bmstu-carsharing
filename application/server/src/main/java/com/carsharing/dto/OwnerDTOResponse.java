package com.carsharing.dto;

public record OwnerDTOResponse(
        int id,
        String firstName,
        String lastName,
        String middleName,
        String phone,
        String email,
        int genderId,
        double rating,
        String passport,
        String password
) {
}
