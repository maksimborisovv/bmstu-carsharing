package com.carsharing.dto;

public record GenderDTORequest(
        String name
) {
}
