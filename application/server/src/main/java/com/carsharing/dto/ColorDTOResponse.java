package com.carsharing.dto;

public record ColorDTOResponse(
        int id,
        String name
) {
}
