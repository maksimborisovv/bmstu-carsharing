package com.carsharing.dto;

public record CategoryDTORequest(
        String name
) {
}
