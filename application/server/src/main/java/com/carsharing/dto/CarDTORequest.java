package com.carsharing.dto;

public record CarDTORequest(
        String model,
        String plateNumber,
        int prodYear,
        int colorId,
        int categoryId,
        String description,
        String documents,
        int ownerId
) {
}
