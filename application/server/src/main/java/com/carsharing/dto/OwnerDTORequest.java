package com.carsharing.dto;

public record OwnerDTORequest(
        String firstName,
        String lastName,
        String middleName,
        String phone,
        String email,
        int genderId,
        double rating,
        String passport,
        String password
) {
}
