package com.carsharing.entity;

import lombok.Builder;

import java.time.LocalDateTime;


@Builder
public class OrderEntity {
    private int id;
    private int offerId;
    private int clientId;
    private LocalDateTime rentalBegin;
    private LocalDateTime rentalEnd;

    public OrderEntity() {}

    public OrderEntity(int id, int offerId, int clientId, LocalDateTime rentalBegin, LocalDateTime rentalEnd) {
        this.id = id;
        this.offerId = offerId;
        this.clientId = clientId;
        this.rentalBegin = rentalBegin;
        this.rentalEnd = rentalEnd;
    }

    public OrderEntity(int offerId, int clientId, LocalDateTime rentalBegin, LocalDateTime rentalEnd) {
        this.offerId = offerId;
        this.clientId = clientId;
        this.rentalBegin = rentalBegin;
        this.rentalEnd = rentalEnd;
    }

    public int getId() {
        return id;
    }

    public int getOfferId() {
        return offerId;
    }

    public int getClientId() {
        return clientId;
    }

    public LocalDateTime getRentalBegin() {
        return rentalBegin;
    }

    public LocalDateTime getRentalEnd() {
        return rentalEnd;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setRentalEnd(LocalDateTime rentalEnd) {
        this.rentalEnd = rentalEnd;
    }
}
