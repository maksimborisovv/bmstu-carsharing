package com.carsharing.entity;


import lombok.Builder;

@Builder
public class ColorEntity {
    private int id;
    private String name;

    public ColorEntity() {}

    public ColorEntity(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public ColorEntity(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
