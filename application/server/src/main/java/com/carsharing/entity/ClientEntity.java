package com.carsharing.entity;


import lombok.Builder;

@Builder
public class ClientEntity {
    private int id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String phone;
    private String email;
    private int genderId;
    private String passport;
    private String drivingLicense;
    private String password;

    public ClientEntity() {}

    public ClientEntity(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public ClientEntity(int id, String firstName, String lastName, String middleName,
                        String phone, String email, int genderId,
                        String passport, String drivingLicense, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.phone = phone;
        this.email = email;
        this.genderId = genderId;
        this.passport = passport;
        this.drivingLicense = drivingLicense;
        this.password = password;
    }

    public ClientEntity(String firstName, String lastName, String middleName,
                        String phone, String email, int genderId,
                        String passport, String drivingLicense, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.phone = phone;
        this.email = email;
        this.genderId = genderId;
        this.passport = passport;
        this.drivingLicense = drivingLicense;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getPassport() {
        return passport;
    }

    public String getDrivingLicense() {
        return drivingLicense;
    }

    public int getGenderId()  {
        return genderId;
    }

    public int getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGenderId(int genderId) {
        this.genderId = genderId;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public void setDrivingLicense(String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    @Override
    public String toString() {
        return "Client: " + firstName + ", " + lastName + ", " +
                middleName + ", " + phone + ", " + email + ", " +
                genderId + ", " + passport + ", " + drivingLicense +
                ", " + password;
    }
}
