package com.carsharing.entity;


import lombok.Builder;

@Builder
public class OwnerEntity {
    private int id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String phone;
    private String email;
    private int genderId;
    private double rating;
    private String passport;

    private String password;

    public OwnerEntity() {}

    public OwnerEntity(int id, String firstName, String lastName, String middleName,
                       String phone, String email, int genderId,
                       double rating, String passport, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.phone = phone;
        this.email = email;
        this.genderId = genderId;
        this.rating = rating;
        this.passport = passport;
        this.password = password;
    }

    public OwnerEntity(String firstName, String lastName, String middleName,
                       String phone, String email, int genderId,
                       double rating, String passport, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.phone = phone;
        this.email = email;
        this.genderId = genderId;
        this.rating = rating;
        this.passport = passport;
        this.password = password;
    }

    public OwnerEntity(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getPassport() {
        return passport;
    }

    public double getRating() {
        return rating;
    }

    public int getGenderId()  {
        return genderId;
    }

    public int getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
