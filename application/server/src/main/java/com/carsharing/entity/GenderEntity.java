package com.carsharing.entity;


import lombok.Builder;

@Builder
public class GenderEntity {
    private int id;
    private String name;

    public GenderEntity() {}

    public GenderEntity(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public GenderEntity(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
