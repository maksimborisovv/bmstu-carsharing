package com.carsharing.entity;


import lombok.Builder;

@Builder
public class OfferEntity {
    private int id;
    private int carId;
    private int ownerId;
    private int price;
    private boolean availability;

    public OfferEntity() {}

    public OfferEntity(int id, int carId, int ownerId, int price, boolean availability) {
        this.id = id;
        this.carId = carId;
        this.ownerId = ownerId;
        this.price = price;
        this.availability = availability;
    }

    public OfferEntity(int carId, int ownerId, int price, boolean availability) {
        this.carId = carId;
        this.ownerId = ownerId;
        this.price = price;
        this.availability = availability;
    }

    public int getId() {
        return id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public int getCarId() {
        return carId;
    }

    public int getPrice() {
        return price;
    }

    public boolean getAvailability() {
        return availability;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public String toString() {
        return "Offer: " + id + ", " + ownerId + ", " + carId + ", " + price + ", " + availability;
    }
}
