package com.carsharing.entity;

import lombok.Builder;

@Builder
public class CarEntity {
    private int id;
    private String model;
    private String plateNumber;
    private int prodYear;
    private int colorId;
    private int categoryId;
    private String description;
    private String documents;
    private int ownerId;

    public CarEntity() {}

    public CarEntity(int id, String model, String plateNumber,
                     int prodYear, int colorId, int categoryId,
                     String description, String documents, int ownerId) {
        this.id = id;
        this.model = model;
        this.plateNumber = plateNumber;
        this.prodYear = prodYear;
        this.colorId = colorId;
        this.categoryId = categoryId;
        this.description = description;
        this.documents = documents;
        this.ownerId = ownerId;
    }

    public CarEntity(String model, String plateNumber,
                     int prodYear, int colorId, int categoryId,
                     String description, String documents, int ownerId) {
        this.model = model;
        this.plateNumber = plateNumber;
        this.prodYear = prodYear;
        this.colorId = colorId;
        this.categoryId = categoryId;
        this.description = description;
        this.documents = documents;
        this.ownerId = ownerId;
    }

    public int getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public String getDescription() {
        return description;
    }

    public String getDocuments() {
        return documents;
    }

    public int getProdYear() {
        return prodYear;
    }

    public int getColorId() {
        return colorId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public void setProdYear(int prodYear) {
        this.prodYear = prodYear;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }
}