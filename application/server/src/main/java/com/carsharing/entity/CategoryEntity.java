package com.carsharing.entity;

import lombok.Builder;

@Builder
public class CategoryEntity {
    private int id;
    private String name;

    public CategoryEntity() {};

    public CategoryEntity(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public CategoryEntity(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
