CREATE TABLE "owners" (
  "id" SERIAL PRIMARY KEY,
  "first_name" varchar NOT NULL,
  "last_name" varchar NOT NULL,
  "middle_name" varchar NOT NULL,
  "phone" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "gender_id" int NOT NULL,
  "rating" real NOT NULL,
  "passport" varchar NOT NULL,
  "password" varchar NOT NULL
);

INSERT INTO "owners" ("first_name", "last_name", "middle_name", "phone", "email", "gender_id", "rating", "passport", "password")
VALUES ('Имя', 'Фамииля', 'Отчество', '8123', 'mail@mail', 1, 0, '1234 123456', 'qwerty123');

CREATE TABLE "genders" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL
);

INSERT INTO "genders" ("name")
VALUES ('Мужской'), ('Женский');

CREATE TABLE "cars" (
  "id" SERIAL PRIMARY KEY,
  "model" varchar NOT NULL,
  "plate_number" varchar NOT NULL,
  "prod_year" int NOT NULL,
  "color_id" int NOT NULL,
  "category_id" int NOT NULL,
  "description" text NOT NULL,
  "documents" varchar NOT NULL,
  "owner_id" int NOT NULL
);

CREATE TABLE "colors" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL
);

INSERT INTO "colors" ("name")
VALUES ('Черный'), ('Белый');

CREATE TABLE "categories" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL
);

INSERT INTO "categories" ("name")
VALUES ('Эконом'), ('Бизнес');

CREATE TABLE "clients" (
  "id" SERIAL PRIMARY KEY,
  "first_name" varchar NOT NULL,
  "last_name" varchar NOT NULL,
  "middle_name" varchar NOT NULL,
  "phone" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "gender_id" int NOT NULL,
  "passport" varchar NOT NULL,
  "driving_license" varchar NOT NULL,
  "password" varchar NOT NULL
);

INSERT INTO "clients" ("first_name", "last_name", "middle_name", "phone", "email", "gender_id", "passport", "driving_license", "password")
VALUES ('Имя', 'Фамииля', 'Отчество', '8123', 'mail@mail', 1, '1234 123456', '1232132', 'qwerty123');

CREATE TABLE "offers" (
  "id" SERIAL PRIMARY KEY,
  "car_id" int,
  "owner_id" int NOT NULL,
  "price" int NOT NULL,
  "availability" bool NOT NULL
);

CREATE TABLE "orders" (
  "id" SERIAL PRIMARY KEY,
  "offer_id" int NOT NULL,
  "client_id" int NOT NULL,
  "rental_begin" timestamp NOT NULL,
  "rental_end" timestamp
);

ALTER TABLE "owners" ADD FOREIGN KEY ("gender_id") REFERENCES "genders" ("id");

ALTER TABLE "cars" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

ALTER TABLE "cars" ADD FOREIGN KEY ("color_id") REFERENCES "colors" ("id");

ALTER TABLE "cars" ADD FOREIGN KEY ("owner_id") REFERENCES "owners" ("id");

ALTER TABLE "clients" ADD FOREIGN KEY ("gender_id") REFERENCES "genders" ("id");

ALTER TABLE "offers" ADD FOREIGN KEY ("car_id") REFERENCES "cars" ("id");

ALTER TABLE "offers" ADD FOREIGN KEY ("owner_id") REFERENCES "owners" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("offer_id") REFERENCES "offers" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("client_id") REFERENCES "clients" ("id");
