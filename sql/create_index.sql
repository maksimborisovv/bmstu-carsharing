create index offer_idx on offers(owner_id);

drop index offer_idx;

SELECT * FROM pg_indexes WHERE tablename = 'offers';