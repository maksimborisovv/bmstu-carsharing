create or replace function delete_car()
returns trigger
as
$$
begin
    update offers set (car_id, availability) = (null, false)
    where offers.id in (select offers.id from offers where car_id = old.id);

    return old;
end;
$$
language plpgsql;

drop trigger if exists DeleteCarTrigger on cars;
create trigger DeleteCarTrigger before delete on cars
    for each row execute procedure delete_car();