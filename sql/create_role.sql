create role Client nocreatedb nosuperuser ;
create role CarOwner nocreatedb nosuperuser ;
create role Administrator nocreatedb nosuperuser ;

grant all privileges on clients to Client;
grant usage on all sequences in schema public to Client;
grant select, update on offers, orders, cars to Client;

grant all privileges on owners, cars to CarOwner;
grant select, insert, update on offers to CarOwner;

grant all privileges on clients to Administrator;
grant all privileges on offers to Administrator;
grant all privileges on orders to Administrator;
grant all privileges on owners to Administrator;