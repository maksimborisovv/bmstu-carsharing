CREATE TABLE "owners" (
  "id" SERIAL PRIMARY KEY,
  "first_name" varchar NOT NULL,
  "last_name" varchar NOT NULL,
  "middle_name" varchar NOT NULL,
  "phone" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "gender_id" int NOT NULL,
  "rating" real NOT NULL,
  "passport" varchar NOT NULL,
  "password" varchar NOT NULL
);

CREATE TABLE "genders" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL
);

CREATE TABLE "cars" (
  "id" SERIAL PRIMARY KEY,
  "model" varchar NOT NULL,
  "plate_number" varchar NOT NULL,
  "prod_year" int NOT NULL,
  "color_id" int NOT NULL,
  "category_id" int NOT NULL,
  "description" text NOT NULL,
  "documents" varchar NOT NULL,
  "owner_id" int NOT NULL
);

CREATE TABLE "colors" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL
);

CREATE TABLE "categories" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL
);

CREATE TABLE "clients" (
  "id" SERIAL PRIMARY KEY,
  "first_name" varchar NOT NULL,
  "last_name" varchar NOT NULL,
  "middle_name" varchar NOT NULL,
  "phone" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "gender_id" int NOT NULL,
  "passport" varchar NOT NULL,
  "driving_license" varchar NOT NULL,
  "password" varchar NOT NULL
);

CREATE TABLE "offers" (
  "id" SERIAL PRIMARY KEY,
  "car_id" int,
  "owner_id" int NOT NULL,
  "price" int NOT NULL,
  "availability" bool NOT NULL
);

CREATE TABLE "orders" (
  "id" SERIAL PRIMARY KEY,
  "offer_id" int NOT NULL,
  "client_id" int NOT NULL,
  "rental_begin" timestamp NOT NULL,
  "rental_end" timestamp
);

ALTER TABLE "owners" ADD FOREIGN KEY ("gender_id") REFERENCES "genders" ("id");

ALTER TABLE "cars" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

ALTER TABLE "cars" ADD FOREIGN KEY ("color_id") REFERENCES "colors" ("id");

ALTER TABLE "cars" ADD FOREIGN KEY ("owner_id") REFERENCES "owners" ("id");

ALTER TABLE "clients" ADD FOREIGN KEY ("gender_id") REFERENCES "genders" ("id");

ALTER TABLE "offers" ADD FOREIGN KEY ("car_id") REFERENCES "cars" ("id");

ALTER TABLE "offers" ADD FOREIGN KEY ("owner_id") REFERENCES "owners" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("offer_id") REFERENCES "offers" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("client_id") REFERENCES "clients" ("id");
