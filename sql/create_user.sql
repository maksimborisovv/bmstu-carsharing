drop user clientUser;
drop user ownerUser;
drop user adminUser;

create user clientUser with password 'Client123' in role client ;
create user ownerUser with password 'Owner123' in role carowner ;
create user adminUser with password 'Admin123' in role administrator ;