copy colors(name)
    from 'C:/gencsv/colors.csv'
    NULL as ''
    csv;

copy genders(name)
    from 'C:/gencsv/genders.csv'
    NULL as ''
    csv;

copy clients(first_name, last_name, middle_name, phone, email, gender_id, passport, driving_license, password)
    from 'C:/gencsv/clients.csv'
    NULL as ''
    csv;

copy categories(name)
    from 'C:/gencsv/categories.csv'
    NULL as ''
    csv;

copy owners(first_name, last_name, middle_name, phone, email, gender_id, rating, passport, password)
    from 'C:/gencsv/owners.csv'
    NULL as ''
    csv;

copy cars(model, plate_number, prod_year, color_id, category_id, description, documents, owner_id)
    from 'C:/gencsv/cars.csv'
    NULL as ''
    csv;

copy offers(car_id, owner_id, price, availability)
    from 'C:/gencsv/offers.csv'
    NULL as ''
    csv;

copy orders(offer_id, client_id, rental_begin, rental_end)
    from 'C:/gencsv/orders.csv'
    NULL as ''
    csv;