import random

import requests
from faker import Faker
from googletrans import Translator
from random import randint
import hashlib
from faker_vehicle import VehicleProvider

colorCNT = 5
genderCNT = 2
categoryCNT = 3

clientCNT = 20
ownerCNT = 20
carCNT = 20
offerCNT = 100
orderCNT = 20


def generateCategory():
    outCategory = open("categories.csv", "w", encoding="utf-8")
    outCategory.write("Эконом\n")
    outCategory.write("Комфорт\n")
    outCategory.write("Бизнес\n")
    outCategory.close()


def generateGender():
    outGender = open("genders.csv", "w", encoding="utf-8")
    outGender.write("Муж\n")
    outGender.write("Жен\n")
    outGender.close()


def generateColor():
    outColor = open("colors.csv", "w", encoding="utf-8")
    outColor.write("Красный\n")
    outColor.write("Зелёный\n")
    outColor.write("Синий\n")
    outColor.write("Белый\n")
    outColor.write("Чёрный\n")
    outColor.close()


def generateClient(n: int):
    outClient = open("clients.csv", "w", encoding="utf-8")
    trans = Translator()
    for i in range(n):
        client = requests.get("https://random-data-api.com/api/v2/users").json()
        first_name = trans.translate(client['first_name'], src="en", dest="ru").text
        last_name = trans.translate(client['last_name'], src="en", dest="ru").text
        middle_name = trans.translate(client['first_name'], src="en", dest="ru").text
        phone = client['phone_number']
        email = client['email']
        gender_id = randint(1, 2)
        passport = "**** ******"
        driving_license = "** ** ******"
        password = hashlib.md5(client['password'].encode()).hexdigest()
        outClient.write(first_name + "," +
                        last_name + "," +
                        middle_name + "," +
                        phone + "," +
                        email + "," +
                        str(gender_id) + "," +
                        passport + "," +
                        driving_license + "," +
                        password + "\n")
    outClient.close()


def generateOwner(n: int):
    outOwner = open("owners.csv", "w", encoding="utf-8")
    trans = Translator()
    for i in range(n):
        owner = requests.get("https://random-data-api.com/api/v2/users").json()
        first_name = trans.translate(owner['first_name'], src="en", dest="ru").text
        last_name = trans.translate(owner['last_name'], src="en", dest="ru").text
        middle_name = trans.translate(owner['first_name'], src="en", dest="ru").text
        phone = owner['phone_number']
        email = owner['email']
        gender_id = randint(1, 2)
        rating = round(random.uniform(3, 5), 1)
        passport = "**** ******"
        password = hashlib.md5(owner['password'].encode()).hexdigest()
        outOwner.write(first_name + "," +
                        last_name + "," +
                        middle_name + "," +
                        phone + "," +
                        email + "," +
                        str(gender_id) + "," +
                        str(rating) + "," +
                        passport + "," +
                        password + "\n")
    outOwner.close()


def generateCars(n: int):
    outCars = open("cars.csv", "w", encoding="utf-8")
    fake = Faker()
    fake.add_provider(VehicleProvider)
    for i in range(n):
        model = fake.vehicle_make_model()
        plateNumber = "* *** ** ***"
        prodYear = randint(2012, 2021)
        colorId = randint(1, colorCNT)
        categoryId = randint(1, categoryCNT)
        description = "lalalal"
        documents = "123456"
        ownerId = randint(1, ownerCNT)
        outCars.write(model + "," +
                      plateNumber + "," +
                      str(prodYear) + "," +
                      str(colorId) + "," +
                      str(categoryId) + "," +
                      description + "," +
                      documents + "," +
                      str(ownerId) + "\n")
    outCars.close()


def generateOffer(n: int):
    outOffers = open("offers.csv", "w", encoding="utf-8")
    for i in range(n):
        carId = randint(1, carCNT)
        ownerId = randint(1, ownerCNT)
        price = randint(1, 5) * 1000
        availability = "True"
        outOffers.write(str(carId) + "," +
                        str(ownerId) + "," +
                        str(price) + "," +
                        availability + "\n")
    outOffers.close()


def generateOrder(n: int):
    outOrders = open("orders.csv", "w", encoding="utf-8")
    for i in range(n):
        offerId = randint(1, offerCNT)
        clientId = randint(1, clientCNT)
        rentalBegin = "2022-08-18 12:34:56"
        rentalEnd = "2022-08-18 13:34:56"
        outOrders.write(str(offerId) + "," +
                        str(clientId) + "," +
                        rentalBegin + "," +
                        rentalEnd + "\n")
    outOrders.close()


generateCars(carCNT)
print("CARS")
generateCategory()
print("CATEGORIES")
generateGender()
print("GENDERS")
generateColor()
print("COLORS")
generateClient(clientCNT)
print("CLIENTS")
generateOwner(ownerCNT)
print("OWNERS")
generateOffer(offerCNT)
print("OFFERS")
generateOrder(orderCNT)
print("ORDERS")